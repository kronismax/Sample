package eu.curiousit.jfinance.ui.login;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;
import eu.curiousit.jfinance.R;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

@RunWith(AndroidJUnit4.class) public class LoginAndPerformTransactionTest {

  @Rule public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);
  private String login = "e";
  private String password = "1";

  /**
   * Язык на устройстве должен быть русский
   * А также возможно не выполнение из-за недостатка средств на счету
   */

  @Test public void loginAndIncomeTransactionTest() {
    onView(withId(R.id.loginEmail)).perform(scrollTo(), click());

    onView(withId(R.id.loginEmail)).perform(scrollTo(), replaceText(login), closeSoftKeyboard());

    onView(withId(R.id.loginPassword)).perform(scrollTo(), replaceText(password), closeSoftKeyboard());

    onView(allOf(withId(R.id.server_pick_spinner), withText("Live"))).perform(replaceText("Dev"));

    onView(allOf(withId(R.id.btnLogin), withId(R.id.btnLogin))).perform(scrollTo(), click());

    onView(
        allOf(withId(R.id.fab_expand_menu_button), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(
        click());

    onView(allOf(withId(R.id.income_fab), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(click());

    onView(withId(R.id.amount_edit_text)).perform(replaceText("5.00"), closeSoftKeyboard());

    onView(allOf(withId(R.id.select_account_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(),
        click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 1),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(allOf(withId(R.id.select_user_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(),
        click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 1),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(allOf(withId(R.id.ok_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(), click());

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("5.00 USD"))));

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("Доход"))));

    onView(allOf(instanceOf(TextView.class), withParent(withId(R.id.toolbar)))).check(matches(withText("Операции")));
  }

  @Test public void loginAndExpenseTransactionTest() {
    onView(withId(R.id.loginEmail)).perform(scrollTo(), click());

    onView(withId(R.id.loginEmail)).perform(scrollTo(), replaceText(login), closeSoftKeyboard());

    onView(withId(R.id.loginPassword)).perform(scrollTo(), replaceText(password), closeSoftKeyboard());

    onView(allOf(withId(R.id.server_pick_spinner), withText("Live"))).perform(replaceText("Dev"));

    onView(allOf(withId(R.id.btnLogin), withId(R.id.btnLogin))).perform(scrollTo(), click());

    onView(
        allOf(withId(R.id.fab_expand_menu_button), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(
        click());

    onView(allOf(withId(R.id.expense_fab), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(click());

    onView(withId(R.id.amount_edit_text)).perform(typeText("10.00"), closeSoftKeyboard());

    onView(allOf(withId(R.id.select_account_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(),
        click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 1),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(allOf(withId(R.id.select_category_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(),
        click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 1),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(allOf(withId(R.id.ok_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(), click());

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("10.00 USD"))));

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("Расход"))));

    onView(allOf(instanceOf(TextView.class), withParent(withId(R.id.toolbar)))).check(matches(withText("Операции")));
  }

  @Test public void loginAndExchangeTransactionTest() {
    onView(withId(R.id.loginEmail)).perform(scrollTo(), click());

    onView(withId(R.id.loginEmail)).perform(scrollTo(), replaceText(login), closeSoftKeyboard());

    onView(withId(R.id.loginPassword)).perform(scrollTo(), replaceText(password), closeSoftKeyboard());

    onView(allOf(withId(R.id.server_pick_spinner), withText("Live"))).perform(replaceText("Dev"));

    onView(allOf(withId(R.id.btnLogin), withId(R.id.btnLogin))).perform(scrollTo(), click());

    onView(
        allOf(withId(R.id.fab_expand_menu_button), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(
        click());

    onView(allOf(withId(R.id.exchange_fab), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(click());

    onView(withId(R.id.amount_edit_text)).perform(replaceText("20.00"), closeSoftKeyboard());

    onView(allOf(withId(R.id.select_account_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(),
        click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 0),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(allOf(withId(R.id.ok_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(), click());

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("20.00 USD"))));

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("Обмен"))));

    onView(allOf(instanceOf(TextView.class), withParent(withId(R.id.toolbar)))).check(matches(withText("Операции")));
  }

  @Test public void loginAndTransferTransactionTest() {
    onView(withId(R.id.loginEmail)).perform(scrollTo(), click());

    onView(withId(R.id.loginEmail)).perform(scrollTo(), replaceText(login), closeSoftKeyboard());

    onView(withId(R.id.loginPassword)).perform(scrollTo(), replaceText(password), closeSoftKeyboard());

    onView(allOf(withId(R.id.server_pick_spinner), withText("Live"))).perform(replaceText("Dev"));

    onView(allOf(withId(R.id.btnLogin), withId(R.id.btnLogin))).perform(scrollTo(), click());

    onView(
        allOf(withId(R.id.fab_expand_menu_button), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(
        click());

    onView(allOf(withId(R.id.transfer_fab), withParent(withId(R.id.multiple_actions)), isDisplayed())).perform(click());

    onView(withId(R.id.amount_edit_text)).perform(replaceText("30.00"), closeSoftKeyboard());

    onView(allOf(withId(R.id.select_account_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(),
        click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 0),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(withId(R.id.select_user_card_view)).perform(scrollTo(), click());

    onView(allOf(childAtPosition(allOf(withId(R.id.contentListView), withParent(withId(R.id.contentListViewFrame))), 0),
        isDisplayed())).perform(click());

    onView(allOf(withId(R.id.buttonDefaultPositive), withText("OK"), isDisplayed())).perform(click());

    onView(allOf(withId(R.id.ok_card_view), withParent(withId(R.id.constraintLayout)))).perform(scrollTo(), click());

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("30.00 USD"))));

    onView(withRecyclerView(R.id.operations_recycler_view).atPosition(0)).check(
        matches(hasDescendant(withText("Внешний перевод"))));

    onView(allOf(instanceOf(TextView.class), withParent(withId(R.id.toolbar)))).check(matches(withText("Операции")));
  }

  private static Matcher<View> childAtPosition(final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup && parentMatcher.matches(parent) && view.equals(
            ((ViewGroup) parent).getChildAt(position));
      }
    };
  }

  public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
    return new RecyclerViewMatcher(recyclerViewId);
  }
}
