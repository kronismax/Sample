package eu.curiousit.jfinance;

class JNotLoggedInException extends JFinanceException {
  /**
   * Constructs a new {@code Exception} that includes the current stack trace.
   */
  public JNotLoggedInException() {
    super();
  }

  /**
   * Constructs a new {@code Exception} with the current stack trace and the
   * specified detail message.
   *
   * @param detailMessage the detail message for this exception.
   */
  public JNotLoggedInException(String detailMessage) {
    super(detailMessage);
  }
}
