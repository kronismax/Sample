package eu.curiousit.jfinance.app;

import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import eu.curiousit.jfinance.BuildConfig;
import eu.curiousit.jfinance.data.database.DatabaseHolder;
import eu.curiousit.jfinance.data.database.dao.JFinanceDao;
import eu.curiousit.jfinance.data.interactor.api.WebService;
import eu.curiousit.jfinance.data.interactor.repository.Repository;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module public class AppModule {

  private static volatile OkHttpClient client;

  public AppModule() {
  }

  @Provides @Singleton JFinanceDao provideJfinanceDao() {
    return DatabaseHolder.database().jfinanceDao();
  }

  @Provides @Singleton WebService provideWebService() {
    return new Retrofit.Builder().baseUrl(BuildConfig.API_ENDPOINT)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(getClient())
        .build()
        .create(WebService.class);
  }

  @Provides @Singleton Repository provideRepository(JFinanceDao weatherDAO, WebService webService) {
    return new Repository(webService, weatherDAO);
  }

  @NonNull private static OkHttpClient getClient() {
    OkHttpClient client = AppModule.client;
    if (client == null) {
      client = AppModule.client;
      if (client == null) {
        client = AppModule.client = buildClient();
      }
    }
    return client;
  }

  @NonNull private static OkHttpClient buildClient() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return new OkHttpClient.Builder().addInterceptor(interceptor).build();
  }
}
