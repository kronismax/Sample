package eu.curiousit.jfinance.app;

import android.app.Application;
import com.facebook.stetho.Stetho;
import eu.curiousit.jfinance.data.database.DatabaseHolder;

public class App extends Application {

  private static AppComponent appComponent;

  @Override
  public void onCreate() {
    super.onCreate();
    DatabaseHolder.init(this);

    Stetho.initializeWithDefaults(this);

    this.deleteDatabase("jfinance-database.db");

    appComponent = DaggerAppComponent.builder()
        .appModule(new AppModule())
        .build();
  }

  public static AppComponent getComponent() {
    return appComponent;
  }

}
