package eu.curiousit.jfinance.app;

import dagger.Component;
import eu.curiousit.jfinance.data.database.dao.JFinanceDao;
import eu.curiousit.jfinance.data.interactor.api.WebService;
import eu.curiousit.jfinance.data.interactor.repository.Repository;
import eu.curiousit.jfinance.ui.base.SharedVM;
import eu.curiousit.jfinance.ui.incometransfer.TransactionsViewAdapter;
import eu.curiousit.jfinance.ui.operations.OperationsVM;
import javax.inject.Singleton;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    JFinanceDao jfinanceDao();
    WebService webService();

    Repository repository();

    void inject(Repository repository);
    void inject(TransactionsViewAdapter adapter);
    void inject(OperationsVM operationsVM);
    void inject(SharedVM sharedVM);
}
