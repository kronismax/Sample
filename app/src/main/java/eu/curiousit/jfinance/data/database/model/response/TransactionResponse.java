package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.Transaction;
import java.util.ArrayList;
import java.util.List;

public class TransactionResponse {

  @SerializedName("resultObj") private List<Transaction> mTransactions;
  @SerializedName("message") private String message;
  @SerializedName("successful") private Boolean successful;


  @NonNull public List<Transaction> getTransactions() {
    if (mTransactions == null) {
      return new ArrayList<>();
    }
    return mTransactions;
  }

  public String getMessage() {
    return message;
  }

  public Boolean getSuccessful() {
    return successful;
  }


}
