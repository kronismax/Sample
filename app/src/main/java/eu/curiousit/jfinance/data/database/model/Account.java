package eu.curiousit.jfinance.data.database.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Account {

  @PrimaryKey
  @SerializedName("id")
  @Expose
  private Integer id;

  @SerializedName("description")
  @Expose
  private String description;

  @ColumnInfo(name = "user_id")
  @SerializedName("userId")
  @Expose
  private int userId;
  
  @ColumnInfo(name = "can_be_negative")
  @SerializedName("canBeNegative")
  @Expose
  private Boolean canBeNegative;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Boolean getCanBeNegative() {
    return canBeNegative;
  }

  public void setCanBeNegative(Boolean canBeNegative) {
    this.canBeNegative = canBeNegative;
  }

}
