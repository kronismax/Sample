package eu.curiousit.jfinance.data.database.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;

@Entity
public class Transaction {

  @Ignore
  public static final String STATUS_PENDING = "pending";
  @Ignore
  public static final String STATUS_COMPLETED = "completed";
  @Ignore
  public static final String STATUS_VOID = "void";
  @Ignore
  public static final String STATUS_REVERTED = "reverted";

  @Ignore
  public static final String TYPE_INCOME = "income";
  @Ignore
  public static final String TYPE_EXPENSE = "expense";
  @Ignore
  public static final String TYPE_EXCHANGE = "exchange";
  @Ignore
  public static final String TYPE_INTERNAL_TRANSFER = "internal_transfer";
  @Ignore
  public static final String TYPE_EXTERNAL_TRANSFER = "external_transfer";

  @PrimaryKey
  @SerializedName("id")
  @Expose
  private int mId;

  @ColumnInfo(name = "transaction_status")
  @SerializedName("transactionStatus")
  @Expose
  private String mTransactionStatus;

  @SerializedName("type")
  @Expose
  private String mType;

  @ColumnInfo(name = "income_category_id")
  @SerializedName("incomeCategoryId")
  @Expose
  private int mIncomeCategoryId;

  @ColumnInfo(name = "expense_category_id")
  @SerializedName("expenseCategoryId")
  @Expose
  private int mExpenseCategoryId;

  @ColumnInfo(name = "user_id")
  @SerializedName("userId")
  @Expose
  private int mUserId;

  @ColumnInfo(name = "account_id")
  @SerializedName("accountId")
  @Expose
  private int mAccountId;

  @ColumnInfo(name = "currency_id")
  @SerializedName("currencyId")
  @Expose
  private int mCurrencyId;

  @SerializedName("amount")
  @Expose
  private BigDecimal mAmount;

  @ColumnInfo(name = "destination_user_id")
  @SerializedName("destinationUserId")
  @Expose
  private int mDestinationUserId;

  @ColumnInfo(name = "destination_account_id")
  @SerializedName("destinationAccountId")
  @Expose
  private int mDestinationAccountId;

  @ColumnInfo(name = "destination_currency_id")
  @SerializedName("destinationCurrencyId")
  @Expose
  private int mDestinationCurrencyId;

  @ColumnInfo(name = "destination_amount")
  @SerializedName("destinationAmount")
  @Expose
  private BigDecimal mDestinationAmount;

  @SerializedName("rate")
  @Expose
  private BigDecimal mRate;

  @ColumnInfo(name = "date_time")
  @SerializedName("dateTime")
  @Expose
  private long mDateTime;

  @ColumnInfo(name = "transaction_description")
  @SerializedName("transactionDescription")
  @Expose
  private String mTransactionDescription;

  @SerializedName("contragent")
  @Expose
  private String mContragent;

  @ColumnInfo(name = "income_transfer")
  private int mIncomeTransfer;

  //"id"
      //"transactionStatus"
      //"type"
      //"incomeCategoryId"
      //"expenseCategoryId"
      //"userId"
      //"accountId"
      //"currencyId"
      //"amount"
      //"destinationUserId"
      //"destinationAccountId"
      //"destinationCurrencyId"
      //"destinationAmount"
      //"rate"
      //"dateTime"
      //"transactionDescription"

  public int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  public String getTransactionStatus() {
    return mTransactionStatus;
  }

  public void setTransactionStatus(String transactionStatus) {
    mTransactionStatus = transactionStatus;
  }

  public String getType() {
    return mType;
  }

  public void setType(String type) {
    mType = type;
  }

  public int getIncomeCategoryId() {
    return mIncomeCategoryId;
  }

  public void setIncomeCategoryId(int incomeCategoryId) {
    mIncomeCategoryId = incomeCategoryId;
  }

  public int getExpenseCategoryId() {
    return mExpenseCategoryId;
  }

  public void setExpenseCategoryId(int expenseCategoryId) {
    mExpenseCategoryId = expenseCategoryId;
  }

  public int getUserId() {
    return mUserId;
  }

  public void setUserId(int userId) {
    mUserId = userId;
  }

  public int getAccountId() {
    return mAccountId;
  }

  public void setAccountId(int accountId) {
    mAccountId = accountId;
  }

  public int getCurrencyId() {
    return mCurrencyId;
  }

  public void setCurrencyId(int currencyId) {
    mCurrencyId = currencyId;
  }

  public BigDecimal getAmount() {
    return mAmount;
  }

  public void setAmount(BigDecimal amount) {
    mAmount = amount;
  }

  public int getDestinationUserId() {
    return mDestinationUserId;
  }

  public void setDestinationUserId(int destinationUserId) {
    mDestinationUserId = destinationUserId;
  }

  public int getDestinationAccountId() {
    return mDestinationAccountId;
  }

  public void setDestinationAccountId(int destinationAccountId) {
    mDestinationAccountId = destinationAccountId;
  }

  public int getDestinationCurrencyId() {
    return mDestinationCurrencyId;
  }

  public void setDestinationCurrencyId(int destinationCurrencyId) {
    mDestinationCurrencyId = destinationCurrencyId;
  }

  public BigDecimal getDestinationAmount() {
    return mDestinationAmount;
  }

  public void setDestinationAmount(BigDecimal destinationAmount) {
    mDestinationAmount = destinationAmount;
  }

  public BigDecimal getRate() {
    return mRate;
  }

  public void setRate(BigDecimal rate) {
    mRate = rate;
  }

  public long getDateTime() {
    return mDateTime;
  }

  public void setDateTime(long dateTime) {
    mDateTime = dateTime;
  }

  public String getTransactionDescription() {
    return mTransactionDescription;
  }

  public void setTransactionDescription(String transactionDescription) {
    mTransactionDescription = transactionDescription;
  }

  public String getContragent() {
    return mContragent;
  }

  public void setContragent(String contragent) {
    mContragent = contragent;
  }

  public void setIncomeTransfer(int incomeTransfer) {
    mIncomeTransfer = incomeTransfer;
  }

  public int getIncomeTransfer() {
    return mIncomeTransfer;
  }
}
