package eu.curiousit.jfinance.data.interactor.repository;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.crashlytics.android.Crashlytics;
import eu.curiousit.jfinance.JFinanceException;
import eu.curiousit.jfinance.data.database.dao.JFinanceDao;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.BalanceTotal;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.ExpenseCategory;
import eu.curiousit.jfinance.data.database.model.ExpenseContragent;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.IncomeContragent;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.data.database.model.base.BaseResponse;
import eu.curiousit.jfinance.data.database.model.base.InsertResponse;
import eu.curiousit.jfinance.data.database.model.response.AccountsResponse;
import eu.curiousit.jfinance.data.database.model.response.BalanceResponse;
import eu.curiousit.jfinance.data.database.model.response.BalanceTotalResponse;
import eu.curiousit.jfinance.data.database.model.response.ContragentResponse;
import eu.curiousit.jfinance.data.database.model.response.LoginResponse;
import eu.curiousit.jfinance.data.database.model.response.TransactionResponse;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import eu.curiousit.jfinance.data.database.model.response.UserResponse;
import eu.curiousit.jfinance.data.interactor.api.WebService;
import eu.curiousit.jfinance.utils.Utilities;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public class Repository {

  @NonNull private final WebService service;

  @NonNull private final JFinanceDao jfinanceDao;

  @Inject public Repository(@NonNull WebService service, @NonNull JFinanceDao jfinanceDao) {
    this.service = service;
    this.jfinanceDao = jfinanceDao;
  }

  @NonNull public Observable<UserDataResponse> login(String server, String login, String pass, String lang) {
    return service.login(server, login, pass, lang).map(LoginResponse::getData).doOnNext(response -> {
      jfinanceDao.clearLoginResponse();
      jfinanceDao.saveLoginResponse(response);
    });
  }

  @NonNull public Observable<List<Account>> getAccounts(String sessionUUID) {
    return service.fetchAccounts(sessionUUID)
        .map(AccountsResponse::getAccounts)
        .map((Function<SortedMap<Integer, Account>, List<Account>>) map -> new ArrayList<>(map.values()))
        .doOnNext(accounts -> {
          List<Account> currentAccounts = jfinanceDao.accounts();
          jfinanceDao.clearAccounts(currentAccounts);
          jfinanceDao.saveAccounts(accounts);
        })
        .onErrorResumeNext(throwable -> {
          Crashlytics.logException(throwable);
          List<Account> accounts = jfinanceDao.accounts();
          return Observable.just(accounts);
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<Balance>> getBalances(String sessionUUID) {
    return service.fetchBalances(sessionUUID)
        .map(BalanceResponse::getBalances)
        .map((Function<SortedMap<Integer, Balance>, List<Balance>>) map -> {
          ArrayList<Balance> balances = new ArrayList<>();
          for (Map.Entry<Integer, Balance> entry : map.entrySet()) {
            Balance balance = entry.getValue();
            balance.setId(entry.getKey());
            balances.add(balance);
          }
          return balances;
        })
        .doOnNext(balances -> jfinanceDao.balances().toObservable().subscribe(currentBalance -> {
          jfinanceDao.clearBalances(currentBalance);
          jfinanceDao.saveBalances(balances);
        }))
        .onErrorResumeNext(throwable -> {
          return jfinanceDao.balances().toObservable();
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<BalanceTotal>> getBalancesTotal(String sessionUUID) {
    return service.fetchBalancesTotal(sessionUUID)
        .map(BalanceTotalResponse::getBalances)
        .map((Function<SortedMap<Integer, BalanceTotal>, List<BalanceTotal>>) map -> {
          ArrayList<BalanceTotal> balances = new ArrayList<>();
          for (Map.Entry<Integer, BalanceTotal> entry : map.entrySet()) {
            BalanceTotal balance = new BalanceTotal();
            balance.setAccountId(entry.getValue().getAccountId());
            balance.setBalance(entry.getValue().getBalance());
            balance.setCurrencyId(entry.getValue().getCurrencyId());
            balance.setId(entry.getKey());
            balances.add(balance);
          }
          return balances;
        })
        .doOnNext(balances -> {
          List<BalanceTotal> currentBalances = jfinanceDao.balancesTotal();
          jfinanceDao.clearBalancesTotal(currentBalances);
          jfinanceDao.saveBalancesTotal(balances);
        })
        .onErrorResumeNext(throwable -> {
          List<BalanceTotal> balances = jfinanceDao.balancesTotal();
          return Observable.just(balances);
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<Currency>> getCurrencies(String sessionUUID) {
    return service.fetchCurrencies(sessionUUID)
        .map(response -> {
          if (response.getSuccessful()) {
            return response.getCurrencies();
          } else {
            throw new JFinanceException(
                response.getMessage());// TODO: 8/30/17 пока так но переделать на обертку Response унаследовать все респонсы от него и сделать balanceLiveData типом Response<BaseResponse>
          }
        })
        .map((Function<SortedMap<Integer, Currency>, List<Currency>>) map -> new ArrayList<>(map.values()))
        .doOnNext(currencyList -> jfinanceDao.currencies().subscribe(list -> {
          jfinanceDao.clearCurrencies(list);
          jfinanceDao.saveCurrencies(currencyList);
        }))
        .onErrorResumeNext(throwable -> {
          return jfinanceDao.currencies().toObservable();
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<User>> getUsers(String sessionUUID) {
    return service.fetchUsers(sessionUUID)
        .map(UserResponse::getUsers)
        .map((Function<SortedMap<Integer, User>, List<User>>) map -> new ArrayList<>(map.values()))
        .doOnNext(users -> {
          List<User> currentUsers = jfinanceDao.users();
          jfinanceDao.clearUsers(currentUsers);
          jfinanceDao.saveUsers(users);
        })
        .onErrorResumeNext(throwable -> {
          Crashlytics.logException(throwable);
          List<User> users = jfinanceDao.users();
          return Observable.just(users);
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<Transaction>> getIncomeTransfers(String sessionUUID) {
    return service.fetchIncomeTransfers(sessionUUID).map(transactionResponse -> {
      List<Transaction> transactions = transactionResponse.getTransactions();
      for (Transaction transaction : transactions) {
        transaction.setIncomeTransfer(1);
      }
      return transactions;
    }).doOnNext(transactions -> {
      List<Transaction> currentTransactions = jfinanceDao.incomeTransfers();
      jfinanceDao.clearTransactions(currentTransactions);
      jfinanceDao.saveTransactions(transactions);
    }).onErrorResumeNext(throwable -> {
      Crashlytics.logException(throwable);
      return Observable.just(new ArrayList<>());
    }).doOnError(Crashlytics::logException);
  }

  public Observable<BaseResponse> updateTransaction(String session, Transaction transaction) {
    TreeMap<String, String> params = new TreeMap<>();
    params.put("id", String.valueOf(transaction.getId()));
    if (transaction.getTransactionStatus() != null) params.put("status", transaction.getTransactionStatus());

    if (transaction.getIncomeCategoryId() != 0) {
      params.put("income_category_id", String.valueOf(transaction.getIncomeCategoryId()));
    }
    if (transaction.getExpenseCategoryId() != 0) {
      params.put("expense_category_id", String.valueOf(transaction.getExpenseCategoryId()));
    }
    params.put("account_id", String.valueOf(transaction.getAccountId()));
    params.put("currency_id", String.valueOf(transaction.getCurrencyId()));
    params.put("amount", transaction.getAmount().toString());
    if (transaction.getDestinationUserId() != 0) {
      params.put("destination_user_id", String.valueOf(transaction.getDestinationUserId()));
    }
    if (transaction.getDestinationAccountId() != 0) {
      params.put("destination_account_id", String.valueOf(transaction.getDestinationAccountId()));
    }
    if (transaction.getDestinationCurrencyId() != 0) {
      params.put("destination_currency_id", String.valueOf(transaction.getDestinationCurrencyId()));
    }
    if (transaction.getDestinationAmount() != null) {
      params.put("destination_amount", transaction.getDestinationAmount().toString());
    }
    params.put("rate", transaction.getRate().toString());

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
    params.put("datetime", df.format(Utilities.longToDate(transaction.getDateTime())));

    if (transaction.getTransactionDescription() != null && !transaction.getTransactionDescription().isEmpty()) {
      params.put("transaction_description", transaction.getTransactionDescription());
    }

    return service.updateTransaction(session, params); // TODO: 9/1/17 сделать обертку
  }

  public Observable<InsertResponse> insertTransaction(String session, Transaction tr) {
    TreeMap<String, String> params = new TreeMap<>();
    params.put("sessionUUID", session);
    params.put("type", tr.getType());
    if (tr.getIncomeCategoryId() != 0) {
      params.put("income_category_id", Integer.valueOf(tr.getIncomeCategoryId()).toString());
    }
    if (tr.getExpenseCategoryId() != 0) {
      params.put("expense_category_id", Integer.valueOf(tr.getExpenseCategoryId()).toString());
    }
    params.put("account_id", Integer.valueOf(tr.getAccountId()).toString());
    params.put("currency_id", Integer.valueOf(tr.getCurrencyId()).toString());
    params.put("amount", tr.getAmount().toPlainString());
    if (tr.getDestinationUserId() != 0) {
      params.put("destination_user_id", Integer.valueOf(tr.getDestinationUserId()).toString());
    }
    if (tr.getDestinationAccountId() != 0) {
      params.put("destination_account_id", Integer.valueOf(tr.getDestinationAccountId()).toString());
    }
    if (tr.getDestinationCurrencyId() != 0) {
      params.put("destination_currency_id", Integer.valueOf(tr.getDestinationCurrencyId()).toString());
    }
    if (tr.getDestinationAmount() != null) {
      params.put("destination_amount", tr.getDestinationAmount().toPlainString());
    }
    params.put("rate", tr.getRate().toPlainString());

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
    params.put("datetime", df.format(tr.getDateTime()));

    if (tr.getTransactionDescription() != null && !tr.getTransactionDescription().isEmpty()) {
      params.put("transaction_description", tr.getTransactionDescription());
    }

    if (!TextUtils.isEmpty(tr.getContragent())) {
      params.put("contragent", tr.getContragent());
    }

    return service.insertTransaction(session, params);
  }

  public Observable<BaseResponse> checkSession(String sessionUUID) {
    return service.checkSession(sessionUUID);
  }

  @NonNull public Maybe<List<Transaction>> getTransactions() {
    return jfinanceDao.transactions().firstElement();
  }

  @NonNull public Observable<List<Transaction>> loadTransactions(String sessionUUID, TreeMap<String, String> params) {
    return service.fetchTransactions(sessionUUID, params)
        .map(TransactionResponse::getTransactions)
        .doOnNext(transactions -> {
          List<Transaction> currentTransactions = jfinanceDao.incomeTransfers();
          jfinanceDao.clearTransactions(currentTransactions);
          jfinanceDao.saveTransactions(transactions);
        })
        .onErrorResumeNext(throwable -> {
          Crashlytics.logException(throwable);
          return Observable.just(jfinanceDao.incomeTransfers());
        })
        .doOnError(Crashlytics::logException);
  }

  public Single<UserDataResponse> getLoginResponse() {
    return jfinanceDao.loginResponse();
  }

  @NonNull public Observable<List<IncomeCategory>> getIncomeCategories(String sessionUUID) {
    return service.fetchIncomeCategories(sessionUUID)
        .map(BaseResponse::getObjects)
        .map(
            (Function<SortedMap<Integer, IncomeCategory>, List<IncomeCategory>>) integerIncomeCategoryTreeMap -> new ArrayList<>(
                integerIncomeCategoryTreeMap.values()))
        .doOnNext(incomeCategories -> jfinanceDao.incomeCategories().subscribe(current -> {
          jfinanceDao.clearIncomeCategories(current);
          jfinanceDao.saveIncomeCategories(incomeCategories);
        }))
        .onErrorResumeNext(throwable -> {
          Crashlytics.logException(throwable);
          return jfinanceDao.incomeCategories().toObservable();
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<ExpenseCategory>> getExpenseCategories(String sessionUUID) {
    return service.fetchExpenseCategories(sessionUUID)
        .map(BaseResponse::getObjects)
        .map((Function<SortedMap<Integer, ExpenseCategory>, List<ExpenseCategory>>) categories -> new ArrayList<>(
            categories.values()))
        .doOnNext(incomeCategories -> jfinanceDao.expenseCategories().subscribe(current -> {
          jfinanceDao.clearExpenseCategories(current);
          jfinanceDao.saveExpenseCategories(incomeCategories);
        }))
        .onErrorResumeNext(throwable -> {
          Crashlytics.logException(throwable);
          return jfinanceDao.expenseCategories().toObservable();
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull public Observable<List<Rate>> getRate(String sessionUUID) {
    return service.fetchRates(sessionUUID)
        .map(BaseResponse::getObjects)
        .map((Function<SortedMap<String, Rate>, List<Rate>>) categories -> {
          ArrayList<Rate> rates = new ArrayList<>();
          for (Map.Entry<String, Rate> entry : categories.entrySet()) {
            Rate value = entry.getValue();
            value.setId(entry.getKey());
            rates.add(value);
          }
          return rates;
        })
        .doOnNext(rates -> jfinanceDao.rates().subscribe(current -> {
          jfinanceDao.clearRates(current);
          jfinanceDao.saveRates(rates);
        }))
        .onErrorResumeNext(throwable -> {
          Crashlytics.logException(throwable);
          return jfinanceDao.rates().toObservable();
        })
        .doOnError(Crashlytics::logException);
  }

  @NonNull
  public Observable<List<IncomeContragent>> getContragentsIncome(String sessionUUID, TreeMap<String, String> params) {
    return service.fetchContragentsIncome(sessionUUID, params).map(ContragentResponse::getObjects).map(strings -> {
      List<IncomeContragent> result = new ArrayList<>();
      for (String string : strings) {
        result.add(new IncomeContragent(string));
      }
      return result;
    }).doOnNext(contragents -> jfinanceDao.incomeContragent().subscribe(current -> {
      jfinanceDao.clearIncomeContragent(current);
      jfinanceDao.saveIncomeContragent(contragents);
    })).onErrorResumeNext(throwable -> {
      Crashlytics.logException(throwable);
      return jfinanceDao.incomeContragent().toObservable();
    }).doOnError(Crashlytics::logException);
  }

  @NonNull
  public Observable<List<ExpenseContragent>> getContragentsExpense(String sessionUUID, TreeMap<String, String> params) {
    return service.fetchContragentsExpense(sessionUUID, params).map(ContragentResponse::getObjects).map(strings -> {
      List<ExpenseContragent> result = new ArrayList<>();
      for (String string : strings) {
        result.add(new ExpenseContragent(string));
      }
      return result;
    }).doOnNext(contragents -> jfinanceDao.expenseContragent().subscribe(current -> {
      jfinanceDao.clearExpenseContragent(current);
      jfinanceDao.saveExpenseContragent(contragents);
    })).onErrorResumeNext(throwable -> {
      Crashlytics.logException(throwable);
      return jfinanceDao.expenseContragent().toObservable();
    }).doOnError(Crashlytics::logException);
  }
}
