package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.User;
import java.util.SortedMap;
import java.util.TreeMap;

public class UserResponse {

  @SerializedName("resultObj") private TreeMap<Integer, User> mUsers;

  @NonNull public SortedMap<Integer, User> getUsers() {
    if (mUsers == null) {
      return new TreeMap<>();
    }
    return mUsers;
  }
}
