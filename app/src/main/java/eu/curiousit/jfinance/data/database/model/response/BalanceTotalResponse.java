package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.BalanceTotal;
import java.util.SortedMap;
import java.util.TreeMap;

public class BalanceTotalResponse {

  @SerializedName("resultObj") private TreeMap<Integer, BalanceTotal> mBalances;

  @NonNull public SortedMap<Integer, BalanceTotal> getBalances() {
    if (mBalances == null) {
      return new TreeMap<>();
    }
    return mBalances;
  }
}
