package eu.curiousit.jfinance.data.database.model.response;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.UserLogin;
import java.math.BigDecimal;

@Entity(tableName = "login_response")
public class UserDataResponse {

  @ColumnInfo(name = "creation_time")
  @SerializedName("creationTime")
  @Expose
  private BigDecimal creationTime;

  @ColumnInfo(name = "last_activity")
  @SerializedName("lastActivity")
  @Expose
  private BigDecimal lastActivity;

  @SerializedName("dbname")
  @Expose
  private String dbname;

  @PrimaryKey
  @ColumnInfo(name = "session_uuid")
  @SerializedName("sessionUUID")
  @Expose
  private String sessionUUID;

  @SerializedName("lang")
  @Expose
  private String lang;

  @Embedded
  @SerializedName("user")
  private UserLogin user;

  //    "user":
  //
  //{
  //  "id":5, "admin":true, "login":"elen", "name":"Елена"
  //},

  public BigDecimal getLastActivity() {
    return lastActivity;
  }

  public String getDbname() {
    return dbname;
  }

  public String getSessionUUID() {
    return sessionUUID;
  }

  public BigDecimal getCreationTime() {
    return creationTime;
  }

  public String getLang() {
    return lang;
  }

  public void setCreationTime(BigDecimal creationTime) {
    this.creationTime = creationTime;
  }

  public void setLastActivity(BigDecimal lastActivity) {
    this.lastActivity = lastActivity;
  }

  public void setDbname(String dbname) {
    this.dbname = dbname;
  }

  public void setSessionUUID(String sessionUUID) {
    this.sessionUUID = sessionUUID;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public UserLogin getUser() {
    return user;
  }

  public void setUser(UserLogin user) {
    this.user = user;
  }
}
