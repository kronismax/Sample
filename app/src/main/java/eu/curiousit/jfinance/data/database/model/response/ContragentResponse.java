package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class ContragentResponse<T> {

  @SerializedName("resultObj") private List<T> mObjects;
  @SerializedName("message") private String message;
  @SerializedName("successful") private Boolean successful;

  @NonNull public List<T> getObjects() {
    if (mObjects == null) {
      return new ArrayList<>();
    }
    return mObjects;
  }

  public String getMessage() {
    return message;
  }

  public Boolean getSuccessful() {
    return successful;
  }
}
