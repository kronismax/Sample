package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.Balance;
import java.util.SortedMap;
import java.util.TreeMap;

public class BalanceResponse {

  @SerializedName("resultObj") private TreeMap<Integer, Balance> mBalances;

  @NonNull public SortedMap<Integer, Balance> getBalances() {
    if (mBalances == null) {
      return new TreeMap<>();
    }
    return mBalances;
  }
}
