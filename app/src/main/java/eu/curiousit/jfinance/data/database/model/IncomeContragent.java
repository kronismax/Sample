package eu.curiousit.jfinance.data.database.model;

import android.arch.persistence.room.Entity;
import eu.curiousit.jfinance.data.database.model.base.BaseContragent;

@Entity public class IncomeContragent extends BaseContragent {

  public IncomeContragent() {
  }

  public IncomeContragent(String string) {
    name = string;
  }


}
