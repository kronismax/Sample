package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.Currency;
import java.util.SortedMap;
import java.util.TreeMap;

public class CurrencyResponse {


  @SerializedName("resultObj") private TreeMap<Integer, Currency> mCurrencies;
  @SerializedName("message") private String message;
  @SerializedName("successful") private Boolean successful;


  @NonNull public SortedMap<Integer, Currency> getCurrencies() {
    if (mCurrencies == null) {
      return new TreeMap<>();
    }
    return mCurrencies;
  }

  public String getMessage() {
    return message;
  }

  public Boolean getSuccessful() {
    return successful;
  }
}
