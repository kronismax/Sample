package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

  @SerializedName("resultObj") private UserDataResponse data;
  @SerializedName("message") private String message;
  @SerializedName("successful") private Boolean successful;

  @NonNull public UserDataResponse getData() {
    //if (data == null) {
    //  return new TreeMap();
    //}
    return data;
  }

  public String getMessage() {
    return message;
  }

  public Boolean getSuccessful() {
    return successful;
  }
}
