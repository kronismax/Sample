package eu.curiousit.jfinance.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.BalanceTotal;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.ExpenseCategory;
import eu.curiousit.jfinance.data.database.model.ExpenseContragent;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.IncomeContragent;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import io.reactivex.Flowable;
import io.reactivex.Single;
import java.util.List;

@Dao
public interface JFinanceDao {

    @Query("SELECT * FROM login_response")
    Single<UserDataResponse> loginResponse();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveLoginResponse(UserDataResponse response);

    @Query("DELETE FROM login_response")
    void clearLoginResponse();

    @Query("SELECT * FROM Account")
    List<Account> accounts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAccounts(List<Account> accounts);

    @Delete
    void clearAccounts(List<Account> accounts);

    @Query("SELECT * FROM balance")
    Flowable<List<Balance>> balances();

    @Query("SELECT * FROM balance WHERE id LIKE :id LIMIT 1")
    Single<Balance> getBalanceById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveBalances(List<Balance> balances);

    @Delete
    void clearBalances(List<Balance> balances);

    @Query("SELECT * FROM balancetotal")
    List<BalanceTotal> balancesTotal();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveBalancesTotal(List<BalanceTotal> balances);

    @Delete
    void clearBalancesTotal(List<BalanceTotal> balances);

    @Query("SELECT * FROM currency")
    Flowable<List<Currency>> currencies();

    @Query("SELECT * FROM currency WHERE id LIKE :id LIMIT 1")
    Currency currencyById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveCurrencies(List<Currency> currencies);

    @Delete
    void clearCurrencies(List<Currency> currencies);

    @Query("SELECT * FROM user")
    List<User> users();

    @Query("SELECT * FROM user WHERE id LIKE :id LIMIT 1")
    User userById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveUsers(List<User> users);

    @Delete
    void clearUsers(List<User> users);

    @Query("SELECT * FROM 'Transaction'")
    Flowable<List<Transaction>> transactions();

    @Query("SELECT * FROM 'Transaction' WHERE income_transfer LIKE 1")
    List<Transaction> incomeTransfers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveTransactions(List<Transaction> transactions);

    @Delete
    void clearTransactions(List<Transaction> transactions);

    @Query("SELECT * FROM incomecategory")
    Flowable<List<IncomeCategory>> incomeCategories();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveIncomeCategories(List<IncomeCategory> categories);

    @Delete
    void clearIncomeCategories(List<IncomeCategory> categories);

    @Query("SELECT * FROM expensecategory")
    Flowable<List<ExpenseCategory>> expenseCategories();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveExpenseCategories(List<ExpenseCategory> categories);

    @Delete
    void clearExpenseCategories(List<ExpenseCategory> categories);

    @Query("SELECT * FROM rate")
    Flowable<List<Rate>> rates();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveRates(List<Rate> rates);

    @Delete
    void clearRates(List<Rate> rates);

    @Query("SELECT * FROM incomeContragent")
    Flowable<List<IncomeContragent>> incomeContragent();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveIncomeContragent(List<IncomeContragent> incomeContragents);

    @Delete
    void clearIncomeContragent(List<IncomeContragent> incomeContragent);

    @Query("SELECT * FROM expenseContragent")
    Flowable<List<ExpenseContragent>> expenseContragent();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveExpenseContragent(List<ExpenseContragent> expenseContragents);

    @Delete
    void clearExpenseContragent(List<ExpenseContragent> expenseContragents);
}
