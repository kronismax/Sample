package eu.curiousit.jfinance.data.interactor.api;

import eu.curiousit.jfinance.data.database.model.ExpenseCategory;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.base.BaseResponse;
import eu.curiousit.jfinance.data.database.model.base.InsertResponse;
import eu.curiousit.jfinance.data.database.model.response.AccountsResponse;
import eu.curiousit.jfinance.data.database.model.response.BalanceResponse;
import eu.curiousit.jfinance.data.database.model.response.BalanceTotalResponse;
import eu.curiousit.jfinance.data.database.model.response.ContragentResponse;
import eu.curiousit.jfinance.data.database.model.response.CurrencyResponse;
import eu.curiousit.jfinance.data.database.model.response.LoginResponse;
import eu.curiousit.jfinance.data.database.model.response.TransactionResponse;
import eu.curiousit.jfinance.data.database.model.response.UserResponse;
import io.reactivex.Observable;
import java.util.TreeMap;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface WebService {
    @GET("users/get_session")
    Observable<LoginResponse> login(@Query("dbname") String dbname,
      @Query("login") String login, @Query("password") String password, @Query("lang") String lang);

    @GET("accounts/get")
    Observable<AccountsResponse> fetchAccounts(@Query("sessionUUID") String sessionUUID);

    @GET("balances/get")
    Observable<BalanceResponse> fetchBalances(@Query("sessionUUID") String sessionUUID);

    @GET("balances/get_total")
    Observable<BalanceTotalResponse> fetchBalancesTotal(@Query("sessionUUID") String sessionUUID);

    @GET("currencies/get")
    Observable<CurrencyResponse> fetchCurrencies(@Query("sessionUUID") String sessionUUID);

    @GET("users/get_users_list")
    Observable<UserResponse> fetchUsers(@Query("sessionUUID") String sessionUUID);

    @GET("transactions/get_income_transfers")
    Observable<TransactionResponse> fetchIncomeTransfers(@Query("sessionUUID") String sessionUUID);

    @PUT("transactions/put") Observable<BaseResponse>
    updateTransaction(@Query("sessionUUID") String session, @QueryMap TreeMap<String, String> params);

    @POST("transactions/post") Observable<InsertResponse>
    insertTransaction(@Query("sessionUUID") String session, @QueryMap TreeMap<String, String> params);

    @GET("transactions/get")
    Observable<TransactionResponse> fetchTransactions(@Query("sessionUUID") String session, @QueryMap TreeMap<String, String> params);

    @GET("income_categories/get")
    Observable<BaseResponse<Integer, IncomeCategory>> fetchIncomeCategories(@Query("sessionUUID") String sessionUUID);

    @GET("expense_categories/get")
    Observable<BaseResponse<Integer, ExpenseCategory>> fetchExpenseCategories(@Query("sessionUUID") String sessionUUID);

    @GET("rates/get")
    Observable<BaseResponse<String, Rate>> fetchRates(@Query("sessionUUID") String sessionUUID);

    @GET("contragents/get")
    Observable<ContragentResponse<String>> fetchContragentsIncome(@Query("sessionUUID") String sessionUUID, @QueryMap TreeMap<String, String> params);

    @GET("contragents/get")
    Observable<ContragentResponse<String>> fetchContragentsExpense(@Query("sessionUUID") String sessionUUID, @QueryMap TreeMap<String, String> params);

    @GET("users/check_session")
    Observable<BaseResponse> checkSession(@Query("sessionUUID") String sessionUUID);

}
