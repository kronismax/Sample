package eu.curiousit.jfinance.data.database.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLogin {

  @SerializedName("id")
  @Expose
  private int id;
  @SerializedName("admin")
  @Expose
  private Boolean admin;
  @SerializedName("login")
  @Expose
  private String login;
  @SerializedName("name")
  @Expose
  private String name;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Boolean getAdmin() {
    return admin;
  }

  public void setAdmin(Boolean admin) {
    this.admin = admin;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
