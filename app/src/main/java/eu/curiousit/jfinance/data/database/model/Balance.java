package eu.curiousit.jfinance.data.database.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;

@Entity
public class Balance {

  @PrimaryKey
  private Integer id;

  @ColumnInfo(name = "account_id")
  @SerializedName("accountId")
  @Expose
  private Integer accountId;

  @ColumnInfo(name = "currency_id")
  @SerializedName("currencyId")
  @Expose
  private Integer currencyId;

  @SerializedName("balance")
  @Expose
  private BigDecimal balance;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getAccountId() {
    return accountId;
  }

  public void setAccountId(Integer accountId) {
    this.accountId = accountId;
  }

  public Integer getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Integer currencyId) {
    this.currencyId = currencyId;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

}
