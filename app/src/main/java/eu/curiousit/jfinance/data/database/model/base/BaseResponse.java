package eu.curiousit.jfinance.data.database.model.base;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import java.util.SortedMap;
import java.util.TreeMap;

public class BaseResponse<K, V> {

  @SerializedName("resultObj") private TreeMap<K, V> mObjects;
  @SerializedName("message") private String message;
  @SerializedName("successful") private Boolean successful;

  @NonNull public SortedMap<K, V> getObjects() {
    if (mObjects == null) {
      return new TreeMap<>();
    }
    return mObjects;
  }

  public String getMessage() {
    return message;
  }

  public Boolean getSuccessful() {
    return successful;
  }
}
