package eu.curiousit.jfinance.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import eu.curiousit.jfinance.data.database.dao.JFinanceDao;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.BalanceTotal;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.ExpenseCategory;
import eu.curiousit.jfinance.data.database.model.ExpenseContragent;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.IncomeContragent;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;

@Database(entities = {
    Account.class, Balance.class, Currency.class, BalanceTotal.class, User.class, UserDataResponse.class,
    Transaction.class, IncomeCategory.class, ExpenseCategory.class, Rate.class, IncomeContragent.class,
    ExpenseContragent.class
}, version = 1) @TypeConverters(Converters.class) public abstract class JFinanceDatabase extends RoomDatabase {
  public abstract JFinanceDao jfinanceDao();
}
