package eu.curiousit.jfinance.data.database.model.base;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

public class InsertResponse {

  @SerializedName("resultObj") private Integer mId;
  @SerializedName("message") private String message;
  @SerializedName("successful") private Boolean successful;

  @NonNull public Integer getId() {
    if (mId == null) {
      return 0;
    }
    return mId;
  }

  public String getMessage() {
    return message;
  }

  public Boolean getSuccessful() {
    return successful;
  }
}
