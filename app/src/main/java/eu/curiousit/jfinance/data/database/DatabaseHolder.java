package eu.curiousit.jfinance.data.database;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public class DatabaseHolder {

  private static JFinanceDatabase database;

  @MainThread public static void init(@NonNull Context context) {
    database =
        Room.databaseBuilder(context.getApplicationContext(), JFinanceDatabase.class, "jfinance-database").build();
  }

  @NonNull public static JFinanceDatabase database() {
    return database;
  }
}
