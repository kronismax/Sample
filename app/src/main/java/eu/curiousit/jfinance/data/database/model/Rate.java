package eu.curiousit.jfinance.data.database.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;

@Entity
public class Rate {

  @PrimaryKey
  private String mId;

  @Expose
  @SerializedName("datetime")
  private long mDatetime;

  @Expose
  @SerializedName("rate")
  private BigDecimal mRate;

  @Expose
  @SerializedName("name")
  private String mName;

  public String getId() {
    return mId;
  }

  public void setId(String id) {
    mId = id;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }

  public BigDecimal getRate() {
    return mRate;
  }

  public void setRate(BigDecimal rate) {
    mRate = rate;
  }

  public Long getDatetime() {
    return mDatetime;
  }

  public void setDatetime(Long datetime) {
    mDatetime = datetime;
  }
}
