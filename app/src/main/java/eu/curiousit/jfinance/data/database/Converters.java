package eu.curiousit.jfinance.data.database;

import android.arch.persistence.room.TypeConverter;
import java.math.BigDecimal;

public class Converters {

  @TypeConverter public static Double fromBigDecimal(BigDecimal value) {
    if (value == null) {
      return 0d;
    }
    return value.doubleValue();
  }

  @TypeConverter public static BigDecimal toBigDecimal(Double value) {
    return BigDecimal.valueOf(value);
  }
}
