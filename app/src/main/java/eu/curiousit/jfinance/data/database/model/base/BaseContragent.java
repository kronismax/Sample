package eu.curiousit.jfinance.data.database.model.base;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity public class BaseContragent {

  @PrimaryKey protected String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override public String toString() {
    return name;
  }
}
