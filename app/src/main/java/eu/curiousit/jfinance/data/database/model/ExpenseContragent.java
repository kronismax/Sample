package eu.curiousit.jfinance.data.database.model;

import android.arch.persistence.room.Entity;
import eu.curiousit.jfinance.data.database.model.base.BaseContragent;

@Entity public class ExpenseContragent extends BaseContragent {

  public ExpenseContragent() {
  }

  public ExpenseContragent(String string) {
    name = string;
  }
}
