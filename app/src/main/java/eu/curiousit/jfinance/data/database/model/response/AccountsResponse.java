package eu.curiousit.jfinance.data.database.model.response;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import eu.curiousit.jfinance.data.database.model.Account;
import java.util.SortedMap;
import java.util.TreeMap;

public class AccountsResponse {

  @SerializedName("resultObj") private TreeMap<Integer, Account> mAccounts;

  @NonNull public SortedMap<Integer, Account> getAccounts() {
    if (mAccounts == null) {
      return new TreeMap<>();
    }
    return mAccounts;
  }
}
