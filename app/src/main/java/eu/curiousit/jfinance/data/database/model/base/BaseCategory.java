package eu.curiousit.jfinance.data.database.model.base;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class BaseCategory {


  @Expose
  @SerializedName("description")
  private String description;

  @PrimaryKey
  @Expose
  @SerializedName("id")
  private Integer id;

  @Expose
  @SerializedName("parentCategoryID")
  @ColumnInfo(name = "parent_category_id")
  private Integer parentCategoryID;


  public String getDescription() {
    return description;
  }

  public Integer getId() {
    return id;
  }

  public Integer getParentCategoryID() {
    return parentCategoryID;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setParentCategoryID(Integer parentCategoryID) {
    this.parentCategoryID = parentCategoryID;
  }
}
