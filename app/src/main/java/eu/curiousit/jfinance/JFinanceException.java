package eu.curiousit.jfinance;

public class JFinanceException extends Exception {
  /**
   * Constructs a new {@code Exception} that includes the current stack trace.
   */
  JFinanceException() {
    super();
  }

  /**
   * Constructs a new {@code Exception} with the current stack trace and the
   * specified detail message.
   *
   * @param detailMessage the detail message for this exception.
   */
  public JFinanceException(String detailMessage) {
    super(detailMessage);
  }
}
