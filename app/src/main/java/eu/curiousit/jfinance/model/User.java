package eu.curiousit.jfinance.model;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import eu.curiousit.jfinance.entities.AccountEntity;
import java.util.List;

public class User implements ParentListItem {

  private String mName;
  private int mId;
  private List<AccountEntity> mAccountEntities;

  @Override public List<?> getChildItemList() {
    return mAccountEntities;
  }

  @Override public boolean isInitiallyExpanded() {
    return false;
  }

  public User(String name, int id, List<AccountEntity> accountEntities) {
    this.mName = name;
    mId = id;
    mAccountEntities = accountEntities;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    this.mName = name;
  }

  public int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  public List<AccountEntity> getAccountEntities() {
    return mAccountEntities;
  }

  public void setAccountEntities(List<AccountEntity> accountEntities) {
    mAccountEntities = accountEntities;
  }
}
