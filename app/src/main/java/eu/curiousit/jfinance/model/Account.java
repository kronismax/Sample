package eu.curiousit.jfinance.model;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

public class Account implements ParentListItem {

  private int id;
  private String name;
  private List<Detail> detail;

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List getDetail() {
    return detail;
  }

  public void setDetail(List<Detail> detail) {
    this.detail = detail;
  }

  @Override public List<?> getChildItemList() {
    return detail;
  }

  @Override public boolean isInitiallyExpanded() {
    return false;
  }
}
