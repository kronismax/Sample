package eu.curiousit.jfinance.model;

import eu.curiousit.jfinance.utils.Utilities;
import java.util.Locale;

public class Detail {

  private String mString;

  private String mAfterDotString;

  public Detail(String string) {
    if (Locale.getDefault().getCountry().equals("IL")) {
      this.mString = string.split("\\.")[0];
    } else {
      this.mString = Utilities.formatString(string);
    }
    mAfterDotString = string.split("\\.")[1];
  }

  public String getAfterDotString() {
    if (mAfterDotString.split(" ")[0].length() == 1) mAfterDotString = "0" + mAfterDotString;
    return "." + mAfterDotString;
  }

  public void addItem(String item) {
    this.mString = item;
  }

  public String getString() {
    return mString;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Detail detail = (Detail) o;

    return mString != null ? mString.equals(detail.mString) : detail.mString == null;
  }

  @Override public int hashCode() {
    return mString != null ? mString.hashCode() : 0;
  }
}
