package eu.curiousit.jfinance.ui.balance;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.model.Account;
import eu.curiousit.jfinance.model.Detail;
import eu.curiousit.jfinance.ui.viewholders.AccountViewHolder;
import eu.curiousit.jfinance.ui.viewholders.DetailViewHolder;

public class AccountExpandableAdapter extends ExpandableRecyclerAdapter<AccountViewHolder, DetailViewHolder> {

  private final LayoutInflater mInflater;
  private boolean isParent;

  public AccountExpandableAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList,
      boolean isParent) {
    super(parentItemList);
    mInflater = LayoutInflater.from(context);
    this.isParent = isParent;
  }

  @Override public AccountViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
    View recipeView;
    if (isParent) {
      recipeView = mInflater.inflate(R.layout.item_balance_parent, parentViewGroup, false);
    } else {
      recipeView = mInflater.inflate(R.layout.item_account_parent, parentViewGroup, false);
    }
    return new AccountViewHolder(recipeView);
  }

  @Override public DetailViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
    View ingredientView = mInflater.inflate(R.layout.item_balance_child, childViewGroup, false);
    return new DetailViewHolder(ingredientView);
  }

  @Override
  public void onBindParentViewHolder(AccountViewHolder accountViewHolder, int position, ParentListItem parentListItem) {
    Account account = (Account) parentListItem;
    accountViewHolder.bind(account);
  }

  @Override public void onBindChildViewHolder(DetailViewHolder detailViewHolder, int position, Object childListItem) {
    Detail detail = (Detail) childListItem;
    detailViewHolder.bind(detail);
  }
}