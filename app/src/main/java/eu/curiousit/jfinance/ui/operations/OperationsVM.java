package eu.curiousit.jfinance.ui.operations;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.base.BaseResponse;
import eu.curiousit.jfinance.data.response.Response;
import eu.curiousit.jfinance.ui.base.SharedVM;
import io.reactivex.Observable;

public class OperationsVM extends SharedVM {
  @NonNull private MutableLiveData<Boolean> mDataPresent = new MutableLiveData<>();

  public OperationsVM() {
    if (loginResponse.getValue() == null) {
      checkSession(userDataResponse -> {
        loginResponse.setValue(Response.success(userDataResponse));
        checkDataPresent();
      });
    } else {
      checkDataPresent();
    }
  }

  private void checkDataPresent() {
    if (transactionsLD.getValue() == null
        || currenciesLD.getValue() == null
        || accountsLD.getValue() == null
        || usersLD.getValue() == null
        || incomeCategoriesLD.getValue() == null
        || expenseCategoriesLD.getValue() == null) {
      loadAllData(this::setDataPresent);
    } else {
      setDataPresent();
    }
  }

  private void setDataPresent() {
    mDataPresent.setValue(true);
  }

  @NonNull MutableLiveData<Boolean> getDataPresent() {
    return mDataPresent;
  }

  void updateData() {
    loadAllData(this::setDataPresent);
  }

  Observable<BaseResponse> updateTransaction(Transaction transaction) {
    return mRepository.updateTransaction(loginResponse.getValue().getData().getSessionUUID(), transaction);
  }
}
