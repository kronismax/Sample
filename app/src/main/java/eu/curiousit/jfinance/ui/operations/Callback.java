package eu.curiousit.jfinance.ui.operations;

import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.base.BaseResponse;
import eu.curiousit.jfinance.data.database.model.base.InsertResponse;
import io.reactivex.Observable;

public interface Callback {

  Observable<BaseResponse> updateTransaction(Transaction transaction);

  Observable<InsertResponse> insertTransaction(Transaction transaction);

  void update();
}
