package eu.curiousit.jfinance.ui.login;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.crashlytics.android.Crashlytics;
import com.novoda.merlin.MerlinsBeard;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.ui.base.BaseLifecycleActivity;
import eu.curiousit.jfinance.ui.base.SharedVM;
import eu.curiousit.jfinance.ui.incometransfer.IncomeTransfersActivity;
import eu.curiousit.jfinance.ui.main.MainActivity;
import eu.curiousit.jfinance.utils.Utilities;
import io.fabric.sdk.android.Fabric;
import java.util.List;

public class LoginActivity extends BaseLifecycleActivity {
  private ProgressDialog prgDialog;
  private EditText emailET;
  private EditText pwdET;
  @BindView(R.id.btnLogin) Button loginBtn;
  @BindView(R.id.server_pick_spinner) MaterialBetterSpinner mServerPickSpinner;
  @BindArray(R.array.servers) String[] mServers;
  @BindString(R.string.base_url) String baseAddress;
  @BindString(R.string.error_network) String mErrorNetwork;
  private SharedVM mSharedVM;

  @SuppressLint("SetTextI18n") @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Fabric.with(this, new Crashlytics());
    setContentView(R.layout.activity_login);
    ButterKnife.bind(this);

    mSharedVM = ViewModelProviders.of(this).get(SharedVM.class);
    mSharedVM.getIncomeTransfersLD().observe(this, this::navigateToProperActivity);
    mSharedVM.getLoadingLD().observe(this, isLoading -> {
      if (isLoading) {
        prgDialog.show();
      } else {
        hideDialog();
      }
    });
    emailET = findViewById(R.id.loginEmail);
    pwdET = findViewById(R.id.loginPassword);
    prgDialog = new ProgressDialog(this);
    prgDialog.setMessage("Please wait...");
    prgDialog.setCancelable(false);
    mServerPickSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.select_dialog_item, mServers));
  }

  private void navigateToProperActivity(List<Transaction> listResponse) {
    Intent incomeTransfersIntent;
    if (listResponse != null && !listResponse.isEmpty()) {
      incomeTransfersIntent = new Intent(getApplicationContext(), IncomeTransfersActivity.class);
    } else {
      incomeTransfersIntent = new Intent(getApplicationContext(), MainActivity.class);
    }
    hideDialog();
    incomeTransfersIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(incomeTransfersIntent);
  }

  private void showSnackbar(String text) {
    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT)
        .setActionTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent));
    View snackbarView = snackbar.getView();
    int snackbarTextId = android.support.design.R.id.snackbar_text;
    TextView textView = snackbarView.findViewById(snackbarTextId);
    textView.setTextColor(getResources().getColor(R.color.colorOverlay));
    snackbar.show();
  }

  private void hideDialog() {
    if (prgDialog.isShowing()) {
      prgDialog.hide();
    }
  }

  @Override protected void onResume() {
    super.onResume();

    //mSharedVM.login("jfinance_dev", "maks", Utilities.MD5("1"),
    //    getResources().getConfiguration().locale.getLanguage());// TODO: 8/23/17
  }

  /**
   * Method gets triggered when Login button is clicked
   */
  public void loginUser(View view) {
    String login = emailET.getText().toString();
    String password = pwdET.getText().toString();
    if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {
      if (MerlinsBeard.from(this).isConnected()) {
        String lang = getResources().getConfiguration().locale.getLanguage();
        prgDialog.show();
        String s = mServerPickSpinner.getText().toString().toLowerCase();
        mSharedVM.login("jfinance_" + s, login, Utilities.MD5(password), lang);
      } else {
        Toast.makeText(this, mErrorNetwork, Toast.LENGTH_SHORT).show();
      }
    } else {
      Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG)
          .show();
    }
  }
}
