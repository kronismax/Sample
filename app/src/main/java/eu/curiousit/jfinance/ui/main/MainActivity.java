package eu.curiousit.jfinance.ui.main;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.crashlytics.android.Crashlytics;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.DatabaseHolder;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import eu.curiousit.jfinance.ui.balance.BalanceFragment;
import eu.curiousit.jfinance.ui.base.BaseLifecycleActivity;
import eu.curiousit.jfinance.ui.base.SharedVM;
import eu.curiousit.jfinance.ui.exchange.ExchangeFragment;
import eu.curiousit.jfinance.ui.expense.ExpenseFragment;
import eu.curiousit.jfinance.ui.income.IncomeFragment;
import eu.curiousit.jfinance.ui.login.LoginActivity;
import eu.curiousit.jfinance.ui.operations.OperationsFragment;
import eu.curiousit.jfinance.ui.transfer.TransferFragment;
import eu.curiousit.jfinance.utils.ObservableUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.concurrent.TimeUnit;
import jp.wasabeef.blurry.Blurry;

public class MainActivity extends BaseLifecycleActivity
    implements NavigationView.OnNavigationItemSelectedListener, ActivityInteractionListener {

  @BindView(R.id.nav_view) NavigationView mNvDrawer;
  @BindView(R.id.multiple_actions) FloatingActionsMenu mFloatingActionsMenu;
  @BindView(R.id.log_out_button) Button mLogOutButton;
  @BindView(R.id.drawer_layout) DrawerLayout mDrawer;
  @BindView(R.id.toolbar) Toolbar mToolbar;
  @BindView(R.id.container) RelativeLayout mFragmentContainer;
  @BindView(R.id.income_fab) FloatingActionButton mIncomeFab;
  @BindView(R.id.expense_fab) FloatingActionButton mExpenseFab;
  @BindView(R.id.exchange_fab) FloatingActionButton mExchangeFab;
  @BindView(R.id.transfer_fab) FloatingActionButton mTransferFab;
  @BindView(R.id.image_container) ImageView mImageView;
  Unbinder mUnbinder;
  private String mFragmentName;
  private Disposable mCheckSessionSubscribe;
  private SharedVM mSharedVM;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    mUnbinder = ButterKnife.bind(this);

    initExpandableFAB();

    initView();

    initViewModel();

    fragmentTransaction(BalanceFragment.class, false);
  }

  private void initView() {
    View headerLayout = mNvDrawer.inflateHeaderView(R.layout.nav_header_main);
    TextView mNameHeaderTextView = headerLayout.findViewById(R.id.nameHeaderTextView);
    new ObservableUtils<UserDataResponse>().getObservable(DatabaseHolder.database().jfinanceDao().loginResponse(),
        response -> mNameHeaderTextView.setText(response.getUser().getName()));
    setSupportActionBar(mToolbar);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open,
        R.string.navigation_drawer_close);
    mDrawer.addDrawerListener(toggle);
    toggle.syncState();

    mNvDrawer.setNavigationItemSelectedListener(this);
    mNvDrawer.getMenu().getItem(1).setChecked(true);
  }

  /**
   * Setup blur on background when user press FAB
   */
  private void initExpandableFAB() {
    mFloatingActionsMenu.setOnFloatingActionsMenuUpdateListener(
        new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
          @Override public void onMenuExpanded() {
            removeBlurFromImageView();
            Blurry.with(MainActivity.this).
                radius(10).
                sampling(2).
                capture(mFragmentContainer).
                into(mImageView);
            mImageView.setVisibility(ImageView.VISIBLE);
            mFragmentContainer.setVisibility(ImageView.GONE);
          }

          @Override public void onMenuCollapsed() {
            mImageView.setVisibility(ImageView.GONE);
            removeBlurFromImageView();
            mFragmentContainer.setVisibility(ImageView.VISIBLE);
          }

          private void removeBlurFromImageView() {
            if (mImageView.getDrawable() != null) {
              mImageView.setImageDrawable(null);
            }
          }
        });
  }

  private void initViewModel() {
    mSharedVM = ViewModelProviders.of(this).get(SharedVM.class);
  }

  @OnClick({
      R.id.income_fab, R.id.expense_fab, R.id.exchange_fab, R.id.transfer_fab, R.id.log_out_button
  }) public void onClick(View v) {
    switch (v.getId()) {
      case R.id.income_fab:
        fragmentTransaction(IncomeFragment.class, false);
        break;
      case R.id.expense_fab:
        fragmentTransaction(ExpenseFragment.class, false);
        break;
      case R.id.exchange_fab:
        fragmentTransaction(ExchangeFragment.class, false);
        break;
      case R.id.transfer_fab:
        fragmentTransaction(TransferFragment.class, false);
        break;
      case R.id.log_out_button:
        startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
        break;
      default:
        break;
    }
  }

  @Override public void onBackPressed() {
    if (mDrawer.isDrawerOpen(GravityCompat.START)) {
      mDrawer.closeDrawer(GravityCompat.START);
      return;
    }
    if (mFloatingActionsMenu.isExpanded()) {
      mFloatingActionsMenu.collapse();
      return;
    }
    FragmentManager fm = getSupportFragmentManager();
    if (fm.getBackStackEntryCount() > 0) {
      fm.popBackStack();
      Fragment fragment = fm.getFragments().get(0);
      String title = "";
      Class<? extends Fragment> fragmentClass = fragment.getClass();
      if (fragmentClass == null) {
        super.onBackPressed();
      }
      mFragmentName = fragment.getClass().getSimpleName();
      if (fragmentClass != null) {
        if (fragmentClass.isInstance(IncomeFragment.class)) {
          title = getString(R.string.income_title);
        } else if (fragmentClass.isInstance(ExpenseFragment.class)) {
          title = getString(R.string.expense_title);
        } else if (fragmentClass.isInstance(ExchangeFragment.class)) {
          title = getString(R.string.exchange_title);
        } else if (fragmentClass.isInstance(TransferFragment.class)) {
          title = getString(R.string.transfer_title);
        } else if (fragmentClass.isInstance(OperationsFragment.class)) {
          title = getString(R.string.operations);
          mNvDrawer.getMenu().getItem(0).setChecked(true);
        } else {
          title = getString(R.string.balance);
          mNvDrawer.getMenu().getItem(1).setChecked(true);
        }
      }
      setTitle(title);
    } else {
      super.onBackPressed();
    }
  }

  @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    int i = item.getItemId();
    if (i == R.id.nav_operations) {
      checkNavigationItem(item);
      fragmentTransaction(OperationsFragment.class, true);
    } else if (i == R.id.nav_balance) {
      checkNavigationItem(item);
      fragmentTransaction(BalanceFragment.class, true);
    }
    if (mDrawer.isDrawerOpen(GravityCompat.START)) {
      mDrawer.closeDrawer(GravityCompat.START);
    }
    return true;
  }

  void checkNavigationItem(MenuItem item) {
    item.setChecked(true);
    setTitle(item.getTitle());
  }

  private void fragmentTransaction(Class fragmentClass, boolean updateList) {
    if (fragmentClass.getSimpleName().equals(mFragmentName)) {
      collapseMenu();
      return;
    }
    Fragment fragment = null;
    try {
      fragment = (Fragment) fragmentClass.newInstance();
      if (updateList) {
        Bundle args = new Bundle();
        args.putBoolean("update", true);
        fragment.setArguments(args);
      }
    } catch (Exception e) {
      Crashlytics.logException(e);
    }
    collapseMenu();

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction transaction = fragmentManager.beginTransaction();

    String fragmentName = "";
    if (fragment != null) {
      fragmentName = fragment.getClass().getSimpleName();
    }
    int backStackEntryCount = fragmentManager.getBackStackEntryCount();
    Log.d("TAG", "fragmentTransaction: " + backStackEntryCount);
    boolean b = fragmentName.contains("Income")
        || fragmentName.contains("Expense")
        || fragmentName.contains("Exchange")
        || fragmentName.contains("Operations")
        || fragmentName.contains("Transfer");
    if (b) {
      if (backStackEntryCount < 1) {
        transaction.addToBackStack(null);
      } else if (backStackEntryCount == 1) {
        fragmentManager.popBackStackImmediate();
        transaction.addToBackStack(null);
      }
    }
    transaction.replace(R.id.container, fragment, fragmentName).commit();
    mFragmentName = fragmentName;
    MenuItem item0 = mNvDrawer.getMenu().getItem(0);
    MenuItem item1 = mNvDrawer.getMenu().getItem(1);
    if (fragment instanceof OperationsFragment) {
      item0.setChecked(true);
    } else if (fragment instanceof BalanceFragment) {
      item1.setChecked(true);
    } else {
      item0.setChecked(false);
      item1.setChecked(false);
    }
  }

  private void collapseMenu() {
    if (mFloatingActionsMenu.isExpanded()) {
      mFloatingActionsMenu.collapse();
    }
  }

  @Override protected void onPause() {
    super.onPause();
    if (!mCheckSessionSubscribe.isDisposed()) {
      mCheckSessionSubscribe.dispose();
    }
  }

  @Override protected void onResume() {
    super.onResume();
    mCheckSessionSubscribe = Observable.interval(10, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(aLong -> mSharedVM.checkSession()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(baseResponse -> {
              if (!baseResponse.getSuccessful()) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setTitle(R.string.session_expired);
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("Ok", (arg0, arg1) -> {
                  startActivity(new Intent(MainActivity.this, LoginActivity.class));
                  finish();
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
              }
            }));
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mUnbinder.unbind();
  }

  @Override public void operationsFragmentTransaction() {
    fragmentTransaction(OperationsFragment.class, true);
  }
}
