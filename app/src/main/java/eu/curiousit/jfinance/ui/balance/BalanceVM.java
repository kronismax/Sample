package eu.curiousit.jfinance.ui.balance;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.BalanceTotal;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import eu.curiousit.jfinance.data.response.Response;
import eu.curiousit.jfinance.entities.AccountEntity;
import eu.curiousit.jfinance.model.Detail;
import eu.curiousit.jfinance.ui.base.SharedVM;
import io.reactivex.Observable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BalanceVM extends SharedVM {

  @NonNull private MutableLiveData<Response<List<ParentListItem>>> mAccountsLD = new MutableLiveData<>();
  @NonNull private MutableLiveData<Response<ArrayList<String>>> mBalanceTotalLD = new MutableLiveData<>();

  public BalanceVM() {
    checkSession(userDataResponse -> {
      getLoginResponse().setValue(Response.success(userDataResponse));
      getBalances();
    });
  }

  @MainThread void getBalances() {
    Response<UserDataResponse> loginResponse = getLoginResponse().getValue();
    String s;
    if (loginResponse != null) {
      UserDataResponse data = loginResponse.getData();
      if (data != null) {
        s = data.getSessionUUID();
      } else {
        return;
      }
    } else {
      return;
    }
    createObservable(
        Observable.zip(mRepository.getAccounts(s), mRepository.getBalances(s), mRepository.getCurrencies(s),
            mRepository.getUsers(s), mRepository.getBalancesTotal(s),
            (accounts, balances, currencies, users, balanceTotals) -> {
              accountsLD.postValue(accounts);
              balancesLD.postValue(balances);
              currenciesLD.postValue(currencies);
              usersLD.postValue(users);
              balanceTotalsLD.postValue(balanceTotals);
              return true;
            }), BalanceVM.this::setBalances);
  }

  private void setBalances() {
    List<Balance> balanceList = balancesLD.getValue();
    List<BalanceTotal> balanceTotalList = balanceTotalsLD.getValue();
    List<User> userList = usersLD.getValue();
    List<Account> accountList = accountsLD.getValue();
    List<Currency> currencyList = currenciesLD.getValue();

    if (balanceList == null
        || balanceTotalList == null
        || userList == null
        || accountList == null
        || currencyList == null) {
      getBalances();
    } else {
      Map<Integer, Balance> balanceMap = new TreeMap<>();
      for (Balance balance : balanceList) { // TODO: 8/31/17 create util class for this operation type
        balanceMap.put(balance.getId(), balance);
      }
      Map<Integer, BalanceTotal> balanceTotalMap = new TreeMap<>();
      for (BalanceTotal balance : balanceTotalList) {
        balanceTotalMap.put(balance.getId(), balance);
      }
      ArrayList<ParentListItem> mAccounts = new ArrayList<>();
      ArrayList<ParentListItem> mUsers = new ArrayList<>();
      if (loginResponse.getValue().getData().getUser().getAdmin()) {
        for (User user : userList) {
          ArrayList<AccountEntity> accountEntities = new ArrayList<>();
          for (Account account : accountList) {
            if (account.getUserId().compareTo(user.getId()) == 0) {
              accountEntities.add(new AccountEntity(account.getId(), account.getUserId(),
                  // todo переделать, но не срочно! (не самый лучший вариант)
                  account.getCanBeNegative(), account.getDescription()));
            }
          }
          mUsers.add(new eu.curiousit.jfinance.model.User(user.getName(), user.getId(), accountEntities));
        }
        mAccountsLD.postValue(Response.success(mUsers));
      } else {
        ArrayList<Detail> mDetails = new ArrayList<>();
        for (Account a : accountList) {
          eu.curiousit.jfinance.model.Account account = new eu.curiousit.jfinance.model.Account();
          account.setName(a.getDescription());
          account.setId(a.getId());
          for (Currency c : currencyList) {
            Integer id = a.getId() * 1000 + c.getId();
            Balance b = balanceMap.get(id);
            if (b != null) {
              mDetails.add(new Detail(b.getBalance() + " " + c.getCode()));
            }
          }
          account.setDetail(new ArrayList<>(mDetails));
          mDetails.clear();
          mAccounts.add(account);
        }
        mAccountsLD.postValue(Response.success(mAccounts));
      }

      ArrayList<String> totalList = new ArrayList<>();
      for (Currency c : currencyList) {
        Integer id = c.getId();
        BalanceTotal b = balanceTotalMap.get(id);
        if (b != null) {
          totalList.add(b.getBalance() + " " + c.getCode());
        } else {
          totalList.add("0.00 " + c.getCode());
        }
      }

      mBalanceTotalLD.postValue(Response.success(totalList));
    }
  }

  @NonNull @MainThread MutableLiveData<Response<List<ParentListItem>>> getBalance() {
    return mAccountsLD;
  }

  @NonNull @MainThread MutableLiveData<Response<ArrayList<String>>> getBalanceTotalLD() {
    return mBalanceTotalLD;
  }
}
