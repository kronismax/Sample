package eu.curiousit.jfinance.ui.balance;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.DatabaseHolder;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.entities.AccountEntity;
import eu.curiousit.jfinance.model.Account;
import eu.curiousit.jfinance.model.Detail;
import eu.curiousit.jfinance.model.User;
import eu.curiousit.jfinance.ui.viewholders.AccountChildViewHolder;
import eu.curiousit.jfinance.ui.viewholders.UserViewHolder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class UsersExpandableAdapter extends ExpandableRecyclerAdapter<UserViewHolder, AccountChildViewHolder> {

  private final LayoutInflater mInflater;

  public UsersExpandableAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList) {
    super(parentItemList);
    mInflater = LayoutInflater.from(context);
  }

  @Override public UserViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
    View userView = mInflater.inflate(R.layout.item_balance_parent, parentViewGroup, false);
    return new UserViewHolder(userView);
  }

  @Override public AccountChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
    View accountView = mInflater.inflate(R.layout.item_account_child, childViewGroup, false);
    return new AccountChildViewHolder(accountView);
  }

  @Override
  public void onBindParentViewHolder(UserViewHolder accountViewHolder, int position, ParentListItem parentListItem) {
    User account = (User) parentListItem;
    accountViewHolder.bind(account);
  }

  @Override
  public void onBindChildViewHolder(AccountChildViewHolder detailViewHolder, int position, Object childListItem) {
    DatabaseHolder.database()
        .jfinanceDao()
        .currencies()
        .firstElement()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(currencies -> DatabaseHolder.database()
            .jfinanceDao()
            .balances()
            .firstElement()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(balances -> {
              ArrayList<ParentListItem> mAccounts = new ArrayList<>();
              ArrayList<Detail> mDetails = new ArrayList<>();
              AccountEntity detail = (AccountEntity) childListItem;
              Account account = new Account();
              account.setName(detail.getDescription());
              account.setId(detail.getId());
              Map<Integer, Balance> balanceTotalMap = new TreeMap<>();
              for (Balance balance : balances) {
                balanceTotalMap.put(balance.getId(), balance);
              }
              for (Currency c : currencies) {
                Integer id = detail.getId() * 1000 + c.getId();
                Balance b = balanceTotalMap.get(id);
                if (b != null) {
                  mDetails.add(new Detail(b.getBalance() + " " + c.getCode()));
                }
              }
              account.setDetail(new ArrayList<>(mDetails));
              mDetails.clear();
              mAccounts.add(account);
              detailViewHolder.bind(mAccounts);
            }));
  }
}