package eu.curiousit.jfinance.ui.incometransfer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.app.App;
import eu.curiousit.jfinance.data.database.dao.JFinanceDao;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import eu.curiousit.jfinance.data.interactor.repository.Repository;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;

public class TransactionsViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private final List<Transaction> mTransactions;
  private Context mContext;
  @Inject JFinanceDao dao;
  @Inject Repository mRepository;

  public TransactionsViewAdapter(List<Transaction> transactions, Context context) {
    App.getComponent().inject(this);
    mTransactions = transactions;
    mContext = context;
  }

  @Override public IncomeTransactionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.income_external_transfer, parent, false);
    return new IncomeTransactionsViewHolder(view);
  }

  @Override public int getItemCount() {
    return null != mTransactions ? mTransactions.size() : 0;
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    Transaction transaction = mTransactions.get(holder.getAdapterPosition());
    IncomeTransactionsViewHolder mHolder = (IncomeTransactionsViewHolder) holder;

    Observable<String> currencyString =
        Observable.create(e -> e.onNext(dao.currencyById(transaction.getCurrencyId()).getCode()));
    Observable<String> userString = Observable.create(e -> e.onNext(dao.userById(transaction.getUserId()).getName()));

    Observable.zip(currencyString, userString, (s, s2) -> {
      String[] arr = new String[2];
      arr[0] = s;
      arr[1] = s2;
      return arr;
    }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(strings -> {
      mHolder.txtCurrencyCode.setText(strings[0]);
      mHolder.txtNameFrom.setText(strings[1]);
    });
    mHolder.txtAmount.setText(transaction.getAmount().toPlainString());
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    mHolder.txtDate.setText(simpleDateFormat.format(transaction.getDateTime()));
    mHolder.btnAccept.setOnClickListener(view -> showAccountDialog(transaction, mHolder, holder.getAdapterPosition()));
  }

  private class IncomeTransactionsViewHolder extends RecyclerView.ViewHolder {
    final TextView txtDate;
    final TextView txtAmount;
    final TextView txtCurrencyCode;
    final TextView txtNameFrom;
    private Button btnAccept;

    IncomeTransactionsViewHolder(View view) {
      super(view);
      this.txtDate = view.findViewById(R.id.txtDateExternal);
      this.txtAmount = view.findViewById(R.id.txtAmountExternal);
      this.txtCurrencyCode = view.findViewById(R.id.txtCurrencyExternal);
      this.txtNameFrom = view.findViewById(R.id.txtNameExternal);
      this.btnAccept = view.findViewById(R.id.btnAcceptExternal);
    }
  }

  private void showAccountDialog(final Transaction transaction, final IncomeTransactionsViewHolder holder,
      final int position) {
    holder.btnAccept.setClickable(false);
    ArrayList<String> myAccounts = new ArrayList<>();
    ArrayList<Account> allAccounts = new ArrayList<>();
    Observable<List<Account>> accounts = Observable.create(e -> e.onNext(dao.accounts()));
    Observable<UserDataResponse> userData = dao.loginResponse().toObservable();

    Observable.zip(accounts, userData, (accounts1, userDataResponse) -> {
      for (Account account : accounts1) {
        allAccounts.add(account);
        if (account.getUserId().compareTo(userDataResponse.getUser().getId()) == 0) {
          myAccounts.add(account.getDescription());
        }
      }
      return userDataResponse;
    })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            userDataResponse -> new MaterialDialog.Builder(mContext).title(mContext.getString(R.string.select_title))
                .items(myAccounts)
                .itemsCallbackSingleChoice(0, (dialog, view, which, text) -> {
                  for (Account accountEntity : allAccounts) {
                    if (accountEntity.getDescription().equals(text)) {
                      transaction.setDestinationAccountId(accountEntity.getId());
                    }
                  }
                  transaction.setDestinationAmount(transaction.getAmount());
                  transaction.setDestinationCurrencyId(transaction.getCurrencyId());
                  transaction.setTransactionStatus(Transaction.STATUS_COMPLETED);
                  mRepository.updateTransaction(userDataResponse.getSessionUUID(), transaction)
                      .subscribeOn(Schedulers.io())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(aBoolean -> {
                        holder.btnAccept.setClickable(true);
                        if (aBoolean.getSuccessful()) {
                          mTransactions.remove(position);
                          TransactionsViewAdapter.this.notifyItemRemoved(position);
                          TransactionsViewAdapter.this.notifyItemRangeChanged(position, mTransactions.size());
                        }
                      });
                  return true;
                })
                .positiveText(mContext.getString(R.string.ok_text))
                .negativeText(mContext.getString(R.string.cancel))
                .onNegative((dialog, which) -> holder.btnAccept.setClickable(true))
                .cancelListener(dialog -> holder.btnAccept.setClickable(true))
                .show());
  }
}
