package eu.curiousit.jfinance.ui.base;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import com.crashlytics.android.Crashlytics;
import eu.curiousit.jfinance.app.App;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.BalanceTotal;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.ExpenseCategory;
import eu.curiousit.jfinance.data.database.model.ExpenseContragent;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.IncomeContragent;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.data.database.model.base.BaseResponse;
import eu.curiousit.jfinance.data.database.model.base.InsertResponse;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import eu.curiousit.jfinance.data.interactor.repository.Repository;
import eu.curiousit.jfinance.data.response.Response;
import eu.curiousit.jfinance.utils.Constants;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import java.util.TreeMap;
import javax.inject.Inject;

/**
 * Общая вью модель в которой храняться и обрабатываются все данные
 */
public class SharedVM extends ViewModel {

  @Inject public Repository mRepository;
  @NonNull protected MutableLiveData<Throwable> errorLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<Boolean> loadingLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<Response<UserDataResponse>> loginResponse = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<Account>> accountsLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<Balance>> balancesLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<BalanceTotal>> balanceTotalsLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<Transaction>> incomeTransfersLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<Transaction>> transactionsLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<Currency>> currenciesLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<User>> usersLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<IncomeCategory>> incomeCategoriesLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<ExpenseCategory>> expenseCategoriesLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<Rate>> rateLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<IncomeContragent>> incomeContragentLD = new MutableLiveData<>();
  @NonNull protected MutableLiveData<List<ExpenseContragent>> expenseContragentLD = new MutableLiveData<>();

  @Inject public SharedVM() {
    App.getComponent().inject(this);
    loadingLD.setValue(false);
    checkSession(userDataResponse -> getLoginResponse().setValue(Response.success(userDataResponse)));
  }

  @MainThread public void login(String server, String login, String pass, String lang) {
    mRepository.login(server, login, pass, lang)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(userDataResponse -> {
          loginResponse.setValue(Response.success(userDataResponse));
          loadAllData(() -> {

          });
        });
  }

  @MainThread protected void loadAllData(Action action) {
    TreeMap<String, String> params = new TreeMap<>();
    Response<UserDataResponse> response = loginResponse.getValue();
    String session;
    if (response != null && response.getData() != null) {
      params.put(Constants.IS_ADMIN, String.valueOf(response.getData().getUser().getAdmin()));
      session = response.getData().getSessionUUID();
    } else {
      errorLD.setValue(new Exception("Сессия не создана или истекла"));
      return;
    }
    createObservable(Observable.zip(mRepository.getAccounts(session), mRepository.getBalances(session),
        mRepository.loadTransactions(session, params), mRepository.getCurrencies(session),
        mRepository.getBalancesTotal(session), mRepository.getIncomeTransfers(session),
        mRepository.getUsers(session), mRepository.getIncomeCategories(session),
        mRepository.getExpenseCategories(session),
        (accounts, balances, transactions, currencies, balanceTotals, incomeTransfers, users, incomeCategories, expenseCategories) -> {
          accountsLD.postValue(accounts);
          balancesLD.postValue(balances);
          balanceTotalsLD.postValue(balanceTotals);
          incomeTransfersLD.postValue(incomeTransfers);
          transactionsLD.postValue(transactions);
          currenciesLD.postValue(currencies);
          usersLD.postValue(users);
          incomeCategoriesLD.postValue(incomeCategories);
          expenseCategoriesLD.postValue(expenseCategories);
          return true;
        }), action);
  }

  protected void createObservable(Observable<Boolean> observable, Action onComplete) {
    observable.observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .doOnSubscribe(disposable -> loadingLD.setValue(true))
        .doAfterTerminate(() -> loadingLD.setValue(false))
        .doOnComplete(onComplete)
        .doOnError(error -> {
          Crashlytics.logException(error);
          errorLD.setValue(error);
        })
        .subscribe();
  }

  protected void checkSession(Consumer<UserDataResponse> userDataResponseConsumer) {
    if (loginResponse.getValue() == null) {
      mRepository.getLoginResponse()
          .observeOn(AndroidSchedulers.mainThread())
          .subscribeOn(Schedulers.io())
          .subscribe(userDataResponseConsumer);
    }
  }

  public Observable<InsertResponse> insertTransactions(Transaction transaction) {
    return mRepository.insertTransaction(loginResponse.getValue().getData().getSessionUUID(), transaction);
  }

  public void loadTransactions() {
    TreeMap<String, String> params = new TreeMap<>();
    Response<UserDataResponse> response = loginResponse.getValue();
    String session;
    if (response != null && response.getData() != null) {
      params.put(Constants.IS_ADMIN, String.valueOf(response.getData().getUser().getAdmin()));
      session = response.getData().getSessionUUID();
    } else {
      Exception value = new Exception("Сессия не создана или истекла");
      errorLD.setValue(value);
      return;
    }
    mRepository.loadTransactions(session, params)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .doAfterTerminate(() -> loadingLD.setValue(false))
        .doOnSubscribe(disposable -> loadingLD.setValue(true))
        .doOnError(error -> errorLD.postValue(error))
        .subscribe(transactions -> transactionsLD.postValue(transactions));
  }

  @MainThread void loadTransactionsData(Action action) {
    TreeMap<String, String> paramsIncome = new TreeMap<>();
    TreeMap<String, String> paramsExpense = new TreeMap<>();
    paramsIncome.put("isIncome", "true");
    paramsExpense.put("isIncome", "false");
    String sessionUUID = loginResponse.getValue().getData().getSessionUUID();
    createObservable(
        Observable.zip(mRepository.getRate(sessionUUID), mRepository.getContragentsIncome(sessionUUID, paramsIncome),
            mRepository.getContragentsExpense(sessionUUID, paramsExpense), mRepository.getCurrencies(sessionUUID),
            mRepository.getAccounts(sessionUUID), mRepository.getUsers(sessionUUID),
            mRepository.getIncomeCategories(sessionUUID), mRepository.getExpenseCategories(sessionUUID), mRepository.getBalances(sessionUUID),
            (rates, contragentsIncome, contragentsExpense, currencies, accounts, users, incomeCategories, expenseCategories, balances) -> {
              rateLD.postValue(rates);
              incomeContragentLD.postValue(contragentsIncome);
              expenseContragentLD.postValue(contragentsExpense);
              currenciesLD.postValue(currencies);
              accountsLD.postValue(accounts);
              usersLD.postValue(users);
              incomeCategoriesLD.postValue(incomeCategories);
              expenseCategoriesLD.postValue(expenseCategories);
              balancesLD.postValue(balances);
              return true;
            }), action);
  }

  public Observable<BaseResponse> checkSession() {
    Response<UserDataResponse> value = loginResponse.getValue();
    if (value !=null) {
      return mRepository.checkSession(value.getData().getSessionUUID());
    }
    return Observable.error(new Exception("Session is empty"));
  }

  @NonNull public MutableLiveData<Response<UserDataResponse>> getLoginResponse() {
    return loginResponse;
  }

  @NonNull public MutableLiveData<Boolean> getLoadingLD() {
    return loadingLD;
  }

  @NonNull public MutableLiveData<List<Transaction>> getIncomeTransfersLD() {
    return incomeTransfersLD;
  }

  @NonNull public MutableLiveData<List<Transaction>> getTransactionsLD() {
    return transactionsLD;
  }

  @NonNull public MutableLiveData<List<Currency>> getCurrenciesLD() {
    return currenciesLD;
  }

  @NonNull public MutableLiveData<List<Account>> getAccountsLD() {
    return accountsLD;
  }

  @NonNull public MutableLiveData<List<User>> getUsersLD() {
    return usersLD;
  }

  @NonNull public MutableLiveData<List<IncomeCategory>> getIncomeCategoriesLD() {
    return incomeCategoriesLD;
  }

  @NonNull public MutableLiveData<List<ExpenseCategory>> getExpenseCategoriesLD() {
    return expenseCategoriesLD;
  }

  @NonNull public MutableLiveData<Throwable> getErrorLD() {
    return errorLD;
  }

  @NonNull public MutableLiveData<List<Rate>> getRateLD() {
    return rateLD;
  }

  @NonNull public MutableLiveData<List<IncomeContragent>> getIncomeContragentLD() {
    return incomeContragentLD;
  }

  @NonNull public MutableLiveData<List<ExpenseContragent>> getExpenseContragentLD() {
    return expenseContragentLD;
  }

  @NonNull public MutableLiveData<List<Balance>> getBalancesLD() {
    return balancesLD;
  }
}
