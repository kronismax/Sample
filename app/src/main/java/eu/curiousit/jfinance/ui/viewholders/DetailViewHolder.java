package eu.curiousit.jfinance.ui.viewholders;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.model.Detail;

public class DetailViewHolder extends ChildViewHolder {

  public @BindView(R.id.child_uah_detail_text_view) TextView uahTextView;
  public @BindView(R.id.child_after_dot_detail_text_view) TextView afterDotTextView;

  public DetailViewHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public void bind(Detail detail) {
    uahTextView.setText(detail.getString());
    afterDotTextView.setText(detail.getAfterDotString());
  }
}