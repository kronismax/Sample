package eu.curiousit.jfinance.ui.viewholders;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.model.User;

public class UserViewHolder extends ParentViewHolder {

  private static final float INITIAL_POSITION = 0.0f;
  private static final float ROTATED_POSITION = 180f;

  @BindView(R.id.parent_list_item_title_account) TextView mAccountTv;
  @BindView(R.id.arrow_expand_iv) ImageView mArrowExpandImageView;

  public UserViewHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public void bind(User account) {
    mAccountTv.setText(account.getName());
  }

  @SuppressLint("NewApi") @Override public void setExpanded(boolean expanded) {
    super.setExpanded(expanded);
    if (expanded) {
      mArrowExpandImageView.setRotation(ROTATED_POSITION);
    } else {
      mArrowExpandImageView.setRotation(INITIAL_POSITION);
    }
  }

  @Override public void onExpansionToggled(boolean expanded) {
    super.onExpansionToggled(expanded);
    RotateAnimation rotateAnimation;
    if (expanded) { // rotate clockwise
      rotateAnimation = new RotateAnimation(ROTATED_POSITION, INITIAL_POSITION, RotateAnimation.RELATIVE_TO_SELF, 0.5f,
          RotateAnimation.RELATIVE_TO_SELF, 0.5f);
    } else { // rotate counterclockwise
      rotateAnimation =
          new RotateAnimation(-1 * ROTATED_POSITION, INITIAL_POSITION, RotateAnimation.RELATIVE_TO_SELF, 0.5f,
              RotateAnimation.RELATIVE_TO_SELF, 0.5f);
    }

    rotateAnimation.setDuration(200);
    rotateAnimation.setFillAfter(true);
    mArrowExpandImageView.startAnimation(rotateAnimation);
  }
}