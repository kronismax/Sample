package eu.curiousit.jfinance.ui.balance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.utils.Utilities;
import java.util.List;
import java.util.Locale;

public class TotalAdapter extends RecyclerView.Adapter<TotalAdapter.ViewHolder> {

  private final List<String> mCurrency;

  public TotalAdapter(List<String> currency) {
    mCurrency = currency;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);
    View contactView = inflater.inflate(R.layout.item_total, parent, false);
    return new ViewHolder(contactView);
  }

  @Override public void onBindViewHolder(ViewHolder viewHolder, int position) {
    String contact = mCurrency.get(position);
    String[] s = contact.split("\\.");
    if (Locale.getDefault().getCountry().equals("IL")) {
      viewHolder.totalTextView.setText(s[0]);
    } else {
      viewHolder.totalTextView.setText(Utilities.formatString(s[0]));
    }
    String secondPartString = "." + s[1];
    if (secondPartString.split(" ")[0].length() == 1) {
      secondPartString = "0" + secondPartString;
    }
    if (secondPartString.split(" ")[0].length() == 2) {
      secondPartString = secondPartString.split(" ")[0] + "0 " + secondPartString.split(" ")[1];
    }

    viewHolder.afterDotText.setText(secondPartString);
  }

  @Override public int getItemCount() {
    return mCurrency.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.total_text_view) TextView totalTextView;
    @BindView(R.id.child_after_dot_total_text_view) TextView afterDotText;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
