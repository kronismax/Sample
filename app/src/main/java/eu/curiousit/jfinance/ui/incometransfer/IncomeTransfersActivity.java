package eu.curiousit.jfinance.ui.incometransfer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.DatabaseHolder;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.ui.main.MainActivity;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.List;

public class IncomeTransfersActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_income_transfers);

    ProgressDialog prgDialog = new ProgressDialog(this);
    prgDialog.setMessage(getString(R.string.wait));
    prgDialog.setCancelable(false);

    RecyclerView recyclerView = findViewById(R.id.recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    Observable.create((ObservableOnSubscribe<List<Transaction>>) e -> e.onNext(
        DatabaseHolder.database().jfinanceDao().incomeTransfers()))
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(dao -> {
          TransactionsViewAdapter transactionsViewAdapter = new TransactionsViewAdapter(dao, this);
          recyclerView.setAdapter(transactionsViewAdapter);
        });
  }

  public void goToMainActivity(View view) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
    finish();
  }
}
