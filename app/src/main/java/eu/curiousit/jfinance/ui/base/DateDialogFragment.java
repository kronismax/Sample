package eu.curiousit.jfinance.ui.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.utils.Utilities;

public class DateDialogFragment extends DialogFragment {
  // TODO: 15.09.16 remove this and write DataPickerDialog
  public static final String TAG_DATE_SELECTED = "date";
  @BindView(R.id.datePicker) DatePicker datePicker;

  @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
    LayoutInflater inflater = getActivity().getLayoutInflater();
    @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.data_dialog_fragment, null);
    ButterKnife.bind(this, view);
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setView(view).setPositiveButton("Ok", (dialog, which) -> {
      Intent intent = new Intent();
      intent.putExtra(TAG_DATE_SELECTED, Utilities.getDateStringFromDatePicker(datePicker));
      getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
    });
    return builder.create();
  }
}
