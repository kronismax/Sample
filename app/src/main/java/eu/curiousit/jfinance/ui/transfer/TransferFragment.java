package eu.curiousit.jfinance.ui.transfer;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.afollestad.materialdialogs.MaterialDialog;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Balance;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.ui.base.BaseFragment;
import eu.curiousit.jfinance.utils.Utilities;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static eu.curiousit.jfinance.R.string.point;

public class TransferFragment extends BaseFragment {

  @BindView(R.id.data_picker_text_view) TextView dataPickerTextView;
  @BindView(R.id.data_picker_card_view) CardView dataPickerCardView;
  @BindView(R.id.amount_edit_text) EditText amountEditText;
  @BindView(R.id.amount_text_in_card_view) TextView amountTextInCardView;
  @BindView(R.id.currency_card_view) CardView amountCardView;
  @BindView(R.id.exchange_edit_text) EditText exchangeEditText;
  @BindView(R.id.exchange_text_in_card_view) TextView exchangeTextInCardView;
  @BindView(R.id.select_account_text_in_card_view) TextView selectAccountTextInCardView;
  @BindView(R.id.select_account_card_view) CardView selectAccountCardView;
  @BindView(R.id.select_user_text_in_card_view) TextView selectUserTextInCardView;
  @BindView(R.id.select_user_card_view) CardView selectUserCardView;
  @BindView(R.id.ok_card_view) CardView okCardView;
  @BindView(R.id.no_money_text_view) TextView noMoneyTextView;
  @BindView(R.id.destination_text_in_card_view) TextView destinationTextInCardView;
  @BindView(R.id.select_destination_card_view) CardView selectDestinationCardView;
  @BindView(R.id.description_edit_text) EditText descriptionEditText;
  @BindString(point) String pointString;
  @BindString(R.string.usd_exchange) String usdExchange;
  @BindString(R.string.self) String selfString;

  private Unbinder mUnbinder;
  private int mCurrencyCode = -1;
  private int mAccountId = -1;
  private int mUserId = -1;
  private List mList;
  private final List<String> mCurrencyList = new ArrayList<>();
  private final List<String> mAccountList = new ArrayList<>();
  private final List<String> mUserList = new ArrayList<>();
  private BigDecimal mAmount;
  private int mDestinationaccountid = -1;
  private String mTransfertype;
  private BigDecimal mRate;

  public TransferFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getActivity().setTitle(getString(R.string.transfer_title));
    View view = inflater.inflate(R.layout.fragment_transfer, container, false);
    mUnbinder = ButterKnife.bind(this, view);

    dataPickerCardView.setOnClickListener(v -> openWeightPicker());
    dataPickerTextView.setText(Utilities.getCurrentDateForView());
    initViewModel();
    mTransactionVM.getDataPresent().observe(this, isPresent -> getDataFromRequestManager());
    return view;
  }

  private void getDataFromRequestManager() {
    List<Currency> currencies = mTransactionVM.getCurrenciesLD().getValue();
    Currency c = currencies.get(0);
    amountTextInCardView.setText(c.getCode());
    mCurrencyCode = c.getId();
    exchangeTextInCardView.setText(String.format("%s%s", c.getCode(), usdExchange));
    for (Account account : mTransactionVM.getAccountsLD().getValue()) {
      if (account.getUserId() == mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()) {
        mAccountList.add(account.getDescription());
      }
    }
    for (Currency currency : currencies) {
      mCurrencyList.add(currency.getCode());
    }
    for (User user : mTransactionVM.getUsersLD().getValue()) {
      if (mTransactionVM.getLoginResponse().getValue().getData().getUser().getName().equals(user.getName())) {
        mUserList.add(getString(R.string.self));
      } else {
        mUserList.add(user.getName());
      }
    }
    setExchangeRate();
  }

  @OnClick({
      R.id.currency_card_view, R.id.select_account_card_view, R.id.select_user_card_view,
      R.id.select_destination_card_view
  }) public void showChoseDialog(View view) {
    final int id = view.getId();
    switch (view.getId()) {
      case R.id.currency_card_view:
        mList = mCurrencyList;
        break;
      case R.id.select_account_card_view:
      case R.id.select_destination_card_view:
        mList = mAccountList;
        break;
      case R.id.select_user_card_view:
        mList = mUserList;
        break;
      default:
        break;
    }
    new MaterialDialog.Builder(getActivity()).title(R.string.select_title)
        .items(mList)
        .itemsCallbackSingleChoice(0, (dialog, view1, which, text) -> {
          String textToView = text + " " + pointString;
          switch (id) {
            case R.id.currency_card_view:
              amountTextInCardView.setText(textToView);
              exchangeTextInCardView.setText(
                  String.format("%s%s", text, getResources().getString(R.string.usd_exchange)));
              for (Currency currency : mTransactionVM.getCurrenciesLD().getValue()) {
                if (currency.getCode().equals(text)) {
                  mCurrencyCode = currency.getId();
                }
              }
              setMoneyToTextView();
              setExchangeRate();
              return true;
            case R.id.select_account_card_view:
              selectAccountTextInCardView.setText(textToView);
              for (Account account : mTransactionVM.getAccountsLD().getValue()) {
                if (account.getDescription().equals(text)
                    && mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()
                    == account.getUserId()) {
                  mAccountId = account.getId();
                }
              }
              setMoneyToTextView();
              return true;
            case R.id.select_user_card_view:
              selectUserTextInCardView.setText(textToView);

              // Searching for destination mUserId
              mUserId = 0;
              for (User user : mTransactionVM.getUsersLD().getValue()) {
                if (user.getName().equals(text)) {
                  mUserId = user.getId();
                  break;
                }
              }
              if (mUserId == 0) {
                mUserId = mTransactionVM.getLoginResponse().getValue().getData().getUser().getId();
                mAccountList.clear();
                for (Account account : mTransactionVM.getAccountsLD().getValue()) {
                  if (account.getUserId() == mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()
                      && account.getId() != mAccountId) {
                    mAccountList.add(account.getDescription());
                  }
                }
              } else {
                mAccountList.clear();
                for (Account account : mTransactionVM.getAccountsLD().getValue()) {
                  if (account.getUserId() == mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()) {
                    mAccountList.add(account.getDescription());
                  }
                }
              }

              if (text.equals(selfString)) {
                selectDestinationCardView.setVisibility(View.VISIBLE);
                mTransfertype = Transaction.TYPE_INTERNAL_TRANSFER;
              } else {
                if (selectDestinationCardView.getVisibility() == View.VISIBLE) {
                  selectDestinationCardView.setVisibility(View.GONE);
                }
                mDestinationaccountid = -1;
                mTransfertype = Transaction.TYPE_EXTERNAL_TRANSFER;
              }
              return true;
            case R.id.select_destination_card_view:
              destinationTextInCardView.setText(textToView);
              for (Account account : mTransactionVM.getAccountsLD().getValue()) {
                if (account.getDescription().equals(text)) {
                  mDestinationaccountid = account.getId();
                }
              }
              return true;
          }
          return true;
        })
        .positiveText(R.string.ok_text)
        .negativeText(R.string.cancel)
        .show();
  }

  private void setMoneyToTextView() {
    if (mAccountId > 0) {
      Integer id = mAccountId * 1000 + mCurrencyCode;
      Balance balance = null;
      for (Balance balance1 : mTransactionVM.getBalancesLD().getValue()) {
        if (balance1.getId().compareTo(id) == 0) {
          balance = balance1;
        }
      }
      //BalanceEntity balance = mBalanceList.get(id);
      if (balance != null) {
        boolean canBeNegative = false;
        for (Account account : mTransactionVM.getAccountsLD().getValue()) {
          if (account.getId() == mAccountId) {
            canBeNegative = account.getCanBeNegative();
          }
        }
        if (balance.getBalance().compareTo(BigDecimal.ZERO) <= 0 && !canBeNegative) {
          noMoneyTextView.setText(R.string.no_money);
        } else {
          noMoneyTextView.setText(String.valueOf(balance.getBalance()));
        }
      }
    }
  }

  @OnClick(R.id.ok_card_view) public void okClick() {
    String amountString = amountEditText.getText().toString();
    if (TextUtils.isEmpty(amountString)) {
      showToast(R.string.amount_error);
      return;
    }
    String rateString = exchangeEditText.getText().toString();
    if (TextUtils.isEmpty(rateString)) {
      showToast(R.string.select_rate_error);
      return;
    }

    mAmount = new BigDecimal(amountString);
    mRate = new BigDecimal(rateString);
    if (mAmount.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.amount_error);
    } else if (mAccountId < 0) {
      showToast(R.string.select_account_error);
    } else if (mRate.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.select_rate_error);
    } else if (mUserId < 0) {
      showToast(R.string.select_user_error);
      // TODO: 10.06.16 check user id if "self" selected
    } else if (mDestinationaccountid < 0 && mTransfertype.equals(Transaction.TYPE_INTERNAL_TRANSFER)) {
      showToast(R.string.select_destination_error);
    } else {
      insertTransaction(createTransaction());
    }
  }

  private Transaction createTransaction() {
    Transaction tr = new Transaction();
    int id = mTransactionVM.getLoginResponse().getValue().getData().getUser().getId();
    if (mTransfertype.equals(Transaction.TYPE_EXTERNAL_TRANSFER)) {
      tr.setType(Transaction.TYPE_EXTERNAL_TRANSFER);
      tr.setDestinationUserId(mUserId);
    } else {
      tr.setType(Transaction.TYPE_INTERNAL_TRANSFER);
      tr.setDestinationAccountId(mDestinationaccountid);
    }
    tr.setDestinationAmount(mAmount);
    tr.setUserId(id);
    tr.setDestinationCurrencyId(mCurrencyCode);
    tr.setCurrencyId(mCurrencyCode);
    tr.setAccountId(mAccountId);
    tr.setDateTime(Utilities.getDateFromString(dateString).getTime());
    tr.setAmount(mAmount);
    tr.setRate(mRate);
    String description = descriptionEditText.getText().toString();
    tr.setTransactionDescription(description);

    return tr;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mUnbinder.unbind();
  }

  private void setExchangeRate() {
    String pair = exchangeTextInCardView.getText().toString().replaceAll("/", "");
    String rate = "";
    List<Rate> value = mTransactionVM.getRateLD().getValue();
    for (Rate r : value) {
      if (r.getId().equals(pair)) {
        rate = r.getRate().toPlainString();
      }
    }
    exchangeEditText.setText(rate);
  }

  @Override public void setDate(String date) {
    dataPickerTextView.setText(Utilities.getStringFromDate(Utilities.getDateFromString(date)));
  }
}
