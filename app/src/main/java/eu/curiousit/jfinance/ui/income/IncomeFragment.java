package eu.curiousit.jfinance.ui.income;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.afollestad.materialdialogs.MaterialDialog;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.ui.base.BaseFragment;
import eu.curiousit.jfinance.utils.Utilities;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class IncomeFragment extends BaseFragment {

  @BindView(R.id.data_picker_text_view) TextView dataPickerTextView;
  @BindView(R.id.data_picker_card_view) CardView dataPickerCardView;
  @BindView((R.id.ok_card_view)) CardView okCardView;
  @BindView(R.id.amount_edit_text) EditText amountEditText;
  @BindView(R.id.exchange_edit_text) EditText exchangeEditText;
  @BindView(R.id.income_amount_text_in_card_view) TextView amountTextInCardView;
  @BindView(R.id.exchange_text_in_card_view) TextView exchangeTextInCardView;
  @BindView(R.id.select_category_card_view) CardView selectCategoryCardView;
  @BindView(R.id.select_user_text_in_card_view) TextView selectCategoryTextInCardView;
  @BindView(R.id.select_account_text_in_card_view) TextView selectAccountTextInCardView;
  @BindView(R.id.description_edit_text) EditText descriptionEditText;
  @BindView(R.id.select_sub_category_card_view) CardView mSelectSubCategoryCardView;
  @BindView(R.id.select_sub_user_text_in_card_view) TextView mSelectSubUserTextInCardView;
  @BindView(R.id.contragent_card_view) CardView mContragentCardView;
  @BindView(R.id.contragent_text_in_card_view) TextView mContragentTextInCardView;
  @BindString(R.string.usd_exchange) String usdExchange;
  @BindString(R.string.point) String pointString;
  @BindString(R.string.select_sub_category) String mSelectSubCategory;
  @BindString(R.string.add_new_contragent) String mAddNewContragent;

  private Unbinder mUnbinder;
  private BigDecimal amount;
  private BigDecimal exchangeRate;
  private final List<String> currencyList = new ArrayList<>();
  private final List<String> accountList = new ArrayList<>();
  private final List<String> categoryList = new ArrayList<>();
  private List list;
  private int currencyCode = -1;
  private int accountId = -1;
  private int categoryId = -1;
  private boolean isSubCategoryAttend;
  private ArrayList<String> subCategoryList = new ArrayList<>();
  private String contragent = null;

  public IncomeFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_income, container, false);
    mUnbinder = ButterKnife.bind(this, view);
    getActivity().setTitle(getString(R.string.income_title));
    dataPickerCardView.setOnClickListener(v -> openWeightPicker());
    dataPickerTextView.setText(Utilities.getCurrentDateForView());
    dateString = Utilities.getCurrentDate();
    initViewModel();
    mTransactionVM.getDataPresent().observe(this, isPresent -> {
      setExchangeRate();
      getDataFromRequestManager();
    });
    return view;
  }

  private void getDataFromRequestManager() {
    List<Currency> currencies = mTransactionVM.getCurrenciesLD().getValue();
    Currency c = currencies.get(0);
    amountTextInCardView.setText(c.getCode());
    currencyCode = c.getId();
    String code = c.getCode() + usdExchange;
    exchangeTextInCardView.setText(code);
    if (usdExchange.contains(c.getCode())) {
      exchangeEditText.setEnabled(false);
    } else {
      exchangeEditText.setEnabled(true);
    }
    for (Account account : mTransactionVM.getAccountsLD().getValue()) {
      if (account.getUserId() == mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()) {
        accountList.add(account.getDescription());
      }
    }
    for (Currency currency : currencies) {
      currencyList.add(currency.getCode());
    }
    for (IncomeCategory incomeCategory : mTransactionVM.getIncomeCategoriesLD().getValue()) {
      if (incomeCategory.getParentCategoryID() == 0) {
        categoryList.add(incomeCategory.getDescription());
      }
    }
  }

  private void setExchangeRate() {
    String pair = exchangeTextInCardView.getText().toString().replaceAll("/", "");
    String rate = "";
    List<Rate> value = mTransactionVM.getRateLD().getValue();
    for (Rate r : value) {
      if (r.getId().equals(pair)) {
        rate = r.getRate().toPlainString();
      }
    }
    exchangeEditText.setText(rate);
  }

  @OnClick({
      R.id.income_amount_card_view, R.id.select_account_card_view, R.id.select_category_card_view,
      R.id.select_sub_category_card_view, R.id.contragent_card_view
  }) public void showChoseDialog(View view) {
    final int id = view.getId();
    switch (view.getId()) {
      case R.id.income_amount_card_view:
        list = currencyList;
        break;
      case R.id.select_account_card_view:
        list = accountList;
        break;
      case R.id.select_category_card_view:
        list = categoryList;
        break;
      case R.id.select_sub_category_card_view:
        list = subCategoryList;
        break;
      case R.id.contragent_card_view:

        list = mTransactionVM.getIncomeContragentLD().getValue();
        break;
      default:
        break;
    }
    showDialog(id);
  }

  private void showDialog(final int id) {
    if (list == null) {
      return;
    }
    new MaterialDialog.Builder(getActivity()).title(R.string.select_title)
        .items(list)
        .itemsCallbackSingleChoice(0, (dialog, view, which, text) -> {
          String textToView = text + " " + pointString;
          switch (id) {
            case R.id.income_amount_card_view:
              amountTextInCardView.setText(textToView);
              if (usdExchange.contains(text)) {
                exchangeEditText.setEnabled(false);
              } else {
                exchangeEditText.setEnabled(true);
              }
              exchangeTextInCardView.setText(String.format("%s%s", text, usdExchange));
              for (Currency currency : mTransactionVM.getCurrenciesLD().getValue()) {
                if (currency.getCode().equals(text)) {
                  currencyCode = currency.getId();
                }
              }
              setExchangeRate();
              return true;
            case R.id.select_account_card_view:
              selectAccountTextInCardView.setText(textToView);
              for (Account account : mTransactionVM.getAccountsLD().getValue()) {
                if (account.getDescription().equals(text)
                    && mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()
                    == account.getUserId()) {
                  accountId = account.getId();
                }
              }
              return true;
            case R.id.select_category_card_view:
              selectCategoryTextInCardView.setText(textToView);
              for (IncomeCategory category : mTransactionVM.getIncomeCategoriesLD().getValue()) {
                if (category.getDescription().equals(text)) {
                  categoryId = category.getId();
                }
              }
              isSubCategoryAttend = false;
              subCategoryList.clear();
              for (IncomeCategory category : mTransactionVM.getIncomeCategoriesLD().getValue()) {
                if (category.getParentCategoryID() == categoryId) {
                  subCategoryList.add(category.getDescription());
                  isSubCategoryAttend = true;
                }
              }
              if (!isSubCategoryAttend) {
                mSelectSubCategoryCardView.setVisibility(CardView.GONE);
                mSelectSubUserTextInCardView.setText(mSelectSubCategory);
              } else {
                mSelectSubCategoryCardView.setVisibility(CardView.VISIBLE);
              }
              return true;
            case R.id.select_sub_category_card_view:
              mSelectSubUserTextInCardView.setText(textToView);
              for (IncomeCategory incomeCategoryEntity : mTransactionVM.getIncomeCategoriesLD().getValue()) {
                if (incomeCategoryEntity.getDescription().equals(text)) {
                  categoryId = incomeCategoryEntity.getId();
                }
              }
              return true;
            case R.id.contragent_card_view:
              if (text.equals(mAddNewContragent)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                View dialogView = View.inflate(getActivity(), R.layout.add_contragent_layout, null);
                builder.setView(dialogView);
                AlertDialog alertDialog = builder.create();
                alertDialog.setCancelable(true);
                EditText editText = dialogView.findViewById(R.id.contragent_edit_text);
                Button okButton = dialogView.findViewById(R.id.ok_button);
                okButton.setOnClickListener(v -> {
                  String text1 = editText.getText().toString();
                  if (TextUtils.isEmpty(text1)) {
                    showToast(R.string.contragent_error);
                  } else {
                    contragent = text1;
                    String text2 = text1 + " " + pointString;
                    mContragentTextInCardView.setText(text2);
                  }
                  alertDialog.dismiss();
                });
                Button cancelButton = dialogView.findViewById(R.id.cancel_button);
                cancelButton.setOnClickListener(v -> alertDialog.dismiss());
                alertDialog.show();
              } else {
                mContragentTextInCardView.setText(textToView);
                contragent = text.toString();
              }
          }
          return true;
        })
        .positiveText(R.string.ok_text)
        .negativeText(R.string.cancel)
        .show();
  }

  @OnClick(R.id.ok_card_view) public void okClick() {
    String amountString = amountEditText.getText().toString();
    String rate = exchangeEditText.getText().toString();
    if (TextUtils.isEmpty(amountString)) {
      showToast(R.string.amount_error);
      return;
    }
    if (TextUtils.isEmpty(rate)) {
      showToast(R.string.select_rate_error);
      return;
    }
    amount = new BigDecimal(amountString);
    exchangeRate = new BigDecimal(rate);
    if (amount.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.amount_error);
    } else if (exchangeRate.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.exchange_error);
    } else if (accountId < 0) {
      showToast(R.string.select_account_error);
    } else if (categoryId < 0) {
      showToast(R.string.select_category_error);
    } else if (mSelectSubCategoryCardView.getVisibility() == View.VISIBLE && mSelectSubUserTextInCardView.getText()
        .toString()
        .equals(mSelectSubCategory)) {
      showToast(R.string.select_sub_category_error);
    } else if (TextUtils.isEmpty(contragent)) {
      showToast(R.string.select_contragent_error);
    } else {
      insertTransaction(createTransaction());
    }
  }

  private Transaction createTransaction() {
    Transaction tr = new Transaction();
    tr.setType(Transaction.TYPE_INCOME);
    tr.setIncomeCategoryId(categoryId);
    tr.setAccountId(accountId);
    tr.setCurrencyId(currencyCode);
    tr.setDateTime(Utilities.getDateFromString(dateString).getTime());
    tr.setAmount(amount);
    tr.setRate(exchangeRate);
    String description = descriptionEditText.getText().toString();
    tr.setTransactionDescription(description);
    if (!TextUtils.isEmpty(contragent)) {
      tr.setContragent(contragent);
    }
    return tr;
  }

  @Override public void insertTransaction(Transaction transaction) {
    super.insertTransaction(transaction);
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mUnbinder.unbind();
  }

  @Override public void setDate(String date) {
    dataPickerTextView.setText(Utilities.getStringFromDate(Utilities.getDateFromString(date)));
  }
}
