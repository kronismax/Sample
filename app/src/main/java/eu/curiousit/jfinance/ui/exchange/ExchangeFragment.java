package eu.curiousit.jfinance.ui.exchange;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import butterknife.Unbinder;
import com.afollestad.materialdialogs.MaterialDialog;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.Rate;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.ui.base.BaseFragment;
import eu.curiousit.jfinance.utils.Utilities;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static eu.curiousit.jfinance.R.id.buying_currency_text_in_card_view;

public class ExchangeFragment extends BaseFragment {

  @BindView(R.id.data_picker_text_view) TextView dataPickerTextView;
  @BindView(R.id.data_picker_card_view) CardView dataPickerCardView;
  @BindView(R.id.amount_edit_text) EditText amountEditText;
  @BindView(R.id.amount_text_in_card_view) TextView currencyTextInCardView;
  @BindView(R.id.currency_card_view) CardView amountCardView;
  @BindView(R.id.exchange_edit_text) EditText exchangeEditText;
  @BindView(R.id.exchange_text_in_card_view) TextView exchangeTextInCardView;
  @BindView(R.id.select_account_text_in_card_view) TextView selectAccountTextInCardView;
  @BindView(R.id.select_account_card_view) CardView selectAccountCardView;
  @BindView(R.id.ok_card_view) CardView okCardView;
  @BindView(buying_currency_text_in_card_view) TextView buyingCurrencyTextInCardView;
  @BindView(R.id.exchange_card_view) CardView exchangeCardView;
  @BindView(R.id.destination_amount_edit_text) EditText destinationAmountEditText;
  @BindView(R.id.description_edit_text) EditText descriptionEditText;
  @BindString(R.string.point) String pointString;
  @BindString(R.string.usd_exchange) String usdExchange;

  private Unbinder mUnbinder;
  private int currencyCode = -1;
  private int accountId = -1;
  private int buyingCurrencyCode = -1;
  private BigDecimal mAmount;
  private BigDecimal mExchangeRate;
  private BigDecimal mDestinationAmount;
  private List mList;
  private final List<String> mCurrencyList = new ArrayList<>();
  private final List<String> accountList = new ArrayList<>();
  private final List<String> mBuyingCurrencyList = new ArrayList<>();
  private boolean mExchangeFlag;
  private TextWatcher mWatcher = new TextWatcher() {
    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      destinationAmountEditText.removeTextChangedListener(destinationAmountWatcher);
    }

    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
      setDestinationAmount();
    }

    @Override public void afterTextChanged(Editable s) {
      destinationAmountEditText.addTextChangedListener(destinationAmountWatcher);
      destinationaWatcherSetted = true;
    }
  };
  private TextWatcher destinationAmountWatcher = new TextWatcher() {
    @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      amountEditText.removeTextChangedListener(mWatcher);
      exchangeEditText.removeTextChangedListener(mWatcher);
    }

    @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      if (TextUtils.isEmpty(destinationAmountEditText.getText().toString()) || TextUtils.isEmpty(
          amountEditText.getText().toString())) {
        return;
      }
      BigDecimal localAmount = new BigDecimal(amountEditText.getText().toString());
      BigDecimal destinationAmount1 = new BigDecimal(destinationAmountEditText.getText().toString());
      if (localAmount.compareTo(BigDecimal.ZERO) <= 0) {
        mDestinationAmount = BigDecimal.ZERO;
      } else {
        if (!mExchangeFlag) {
          mDestinationAmount = localAmount.divide(destinationAmount1, 6, BigDecimal.ROUND_HALF_UP);
          mOriginalRate = new BigDecimal(1).divide(mDestinationAmount, 6, RoundingMode.HALF_UP);
        } else {
          mDestinationAmount = destinationAmount1.divide(localAmount, 6, BigDecimal.ROUND_HALF_UP);
          mOriginalRate = mDestinationAmount;
        }
      }
      exchangeEditText.setText(mDestinationAmount.toPlainString());
    }

    @Override public void afterTextChanged(Editable editable) {
      amountEditText.addTextChangedListener(mWatcher);
      exchangeEditText.addTextChangedListener(mWatcher);
    }
  };
  private BigDecimal mOriginalRate = BigDecimal.ZERO;
  private boolean destinationaWatcherSetted = false;

  public ExchangeFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getActivity().setTitle(R.string.exchange_title);
    View view = inflater.inflate(R.layout.fragment_exchange, container, false);
    mUnbinder = ButterKnife.bind(this, view);

    dataPickerCardView.setOnClickListener(v -> openWeightPicker());
    dataPickerTextView.setText(Utilities.getCurrentDateForView());
    dateString = Utilities.getCurrentDate();
    initViewModel();
    mTransactionVM.getDataPresent().observe(this, isPresent -> {
      getDataFromRequestManager();
      setExchangeRate(false);
    });
    amountEditText.addTextChangedListener(mWatcher);

    return view;
  }

  @OnTouch(R.id.destination_amount_edit_text) public boolean setDestinationAmountWatcher() {
    if (!destinationaWatcherSetted) {
      destinationAmountEditText.addTextChangedListener(destinationAmountWatcher);
      destinationaWatcherSetted = true;
    }
    return false;
  }

  private void setDestinationAmount() {
    String amountString = amountEditText.getText().toString();
    String exchangeString = exchangeEditText.getText().toString();
    if (!TextUtils.isEmpty(amountString) && !TextUtils.isEmpty(exchangeString)) {
      BigDecimal amount = new BigDecimal(amountString);
      BigDecimal exchange = new BigDecimal(exchangeString);
      destinationAmountEditText.setVisibility(EditText.VISIBLE);
      String text;
      if (mExchangeFlag) {
        BigDecimal value = getValue(exchangeEditText.getText().toString());
        mOriginalRate = value.divide(new BigDecimal(1), 6, RoundingMode.HALF_UP);
        text = mOriginalRate.multiply(amount).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
      } else {
        text =
            amount.divide(exchange, 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
        BigDecimal divide = new BigDecimal(text).divide(amount, 6, BigDecimal.ROUND_CEILING);
        mOriginalRate = divide.setScale(6, BigDecimal.ROUND_HALF_UP);
      }
      destinationAmountEditText.setText(text);
    }
  }

  private void getDataFromRequestManager() {
    List<Currency> currencies = mTransactionVM.getCurrenciesLD().getValue();
    Currency c = currencies.get(0);
    Currency c2 = currencies.get(1);
    String text = c.getCode() + " " + pointString;
    currencyTextInCardView.setText(text);
    currencyCode = c.getId();
    buyingCurrencyCode = c2.getId();
    String exchangeText = c.getCode() + "/" + c2.getCode();
    exchangeTextInCardView.setText(exchangeText);
    if (c2.getCode().equals(c.getCode())) {
      exchangeEditText.setEnabled(false);
    } else {
      exchangeEditText.setEnabled(true);
    }
    String text1 = c2.getCode() + " " + pointString;
    buyingCurrencyTextInCardView.setText(text1);
    for (Account account : mTransactionVM.getAccountsLD().getValue()) {
      if (account.getUserId() == mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()) {
        accountList.add(account.getDescription());
      }
    }
    for (Currency currency : currencies) {
      mCurrencyList.add(currency.getCode());
    }
    for (Currency currency : currencies) {
      mBuyingCurrencyList.add(currency.getCode());
    }
  }

  @OnClick({
      R.id.currency_card_view, R.id.select_account_card_view, R.id.buying_currency_card_view
  }) public void showChoseDialog(View view) {
    final int id = view.getId();
    switch (view.getId()) {
      case R.id.currency_card_view:
        mList = mCurrencyList;
        break;
      case R.id.select_account_card_view:
        mList = accountList;
        break;
      case R.id.buying_currency_card_view:
        mList = mBuyingCurrencyList;
        break;
      default:
        break;
    }
    new MaterialDialog.Builder(getActivity()).title(R.string.select_title)
        .items(mList)
        .itemsCallbackSingleChoice(0, (dialog, view1, which, text) -> {
          String textToView = text + " " + pointString;
          switch (id) {
            case R.id.currency_card_view:
              currencyTextInCardView.setText(textToView);
              String exchangeText = text.toString().substring(0, 3) + "/" + buyingCurrencyTextInCardView.getText()
                  .toString()
                  .replace(pointString, "");
              if (currencyTextInCardView.getText()
                  .toString()
                  .equals(buyingCurrencyTextInCardView.getText().toString())) {
                exchangeEditText.setEnabled(false);
              } else {
                exchangeEditText.setEnabled(true);
              }
              exchangeTextInCardView.setText(exchangeText);
              for (Currency currency : mTransactionVM.getCurrenciesLD().getValue()) {
                if (currency.getCode().equals(text)) {
                  currencyCode = currency.getId();
                  break;
                }
              }
              mOriginalRate = BigDecimal.ZERO;
              setExchangeRate(false);
              setDestinationAmount();
              return true;
            case R.id.select_account_card_view:
              selectAccountTextInCardView.setText(textToView);
              for (Account account : mTransactionVM.getAccountsLD().getValue()) {
                if (account.getDescription().equals(text)
                    && mTransactionVM.getLoginResponse().getValue().getData().getUser().getId()
                    == account.getUserId()) {
                  accountId = account.getId();
                }
              }
              return true;
            case R.id.buying_currency_card_view:
              String buyingCurrencyText =
                  currencyTextInCardView.getText().toString().replace(pointString, "").substring(0, 3);
              buyingCurrencyTextInCardView.setText(textToView);
              String text1 = buyingCurrencyText + "/" + text;
              exchangeTextInCardView.setText(text1);
              for (Currency currency : mTransactionVM.getCurrenciesLD().getValue()) {
                if (currency.getCode().equals(text)) {
                  buyingCurrencyCode = currency.getId();
                  break;
                }
              }
              mOriginalRate = BigDecimal.ZERO;
              setExchangeRate(false);
              setDestinationAmount();
              return true;
            default:
              break;
          }
          return true;
        })
        .positiveText(R.string.ok_text)
        .negativeText(R.string.cancel)
        .show();
  }

  @OnClick(R.id.ok_card_view) public void okClick() {
    getAmountAndExchangeRate();
    if (mAmount.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.amount_error);
    } else if (mDestinationAmount.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.destination_amount_error);
    } else if (mExchangeRate.compareTo(BigDecimal.ZERO) <= 0) {
      showToast(R.string.exchange_error);
    } else if (accountId < 0) {
      showToast(R.string.select_account_error);
    } else {
      insertTransaction(createTransaction());
    }
  }

  @OnClick(R.id.exchange_card_view) public void exchangeOnClick() {
    getAmountAndExchangeRate();
    setExchangeRate(mExchangeFlag);
  }

  private void getAmountAndExchangeRate() {
    String amountString = amountEditText.getText().toString();
    String exchangeRateString = exchangeEditText.getText().toString();
    String destionationAmountString = destinationAmountEditText.getText().toString();
    mAmount = getValue(amountString);
    mExchangeRate = getValue(exchangeRateString);
    mDestinationAmount = getValue(destionationAmountString);
  }

  private BigDecimal getValue(String destionationAmountString) {
    if (!TextUtils.isEmpty(destionationAmountString)) {
      return new BigDecimal(destionationAmountString);
    } else {
      return BigDecimal.ZERO;
    }
  }

  private Transaction createTransaction() {
    Transaction tr = new Transaction();
    tr.setType(Transaction.TYPE_EXCHANGE);
    tr.setDestinationCurrencyId(buyingCurrencyCode);
    tr.setDestinationAmount(mDestinationAmount);
    tr.setDestinationAccountId(accountId);
    tr.setAccountId(accountId);
    tr.setCurrencyId(currencyCode);
    tr.setDateTime(Utilities.getDateFromString(dateString).getTime());
    tr.setAmount(mAmount);
    tr.setRate(mOriginalRate.setScale(6, BigDecimal.ROUND_HALF_UP));
    String description = descriptionEditText.getText().toString();
    tr.setTransactionDescription(description);
    return tr;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mUnbinder.unbind();
  }

  private void setExchangeRate(boolean flag) {
    exchangeEditText.removeTextChangedListener(mWatcher);
    exchangeEditText.removeTextChangedListener(mWatcher);
    String pair = exchangeTextInCardView.getText().toString().replaceAll("/", "");
    pair = pair.replaceAll(" ", "");
    String firstCurrency = pair.substring(0, 3);
    String secondCurrency = pair.substring(3, 6);
    BigDecimal secondCurrencyRate;
    BigDecimal divide;
    if (flag) {
      secondCurrencyRate = mOriginalRate;
      divide =
          new BigDecimal(1).divide(secondCurrencyRate, 6, RoundingMode.HALF_UP).setScale(6, BigDecimal.ROUND_HALF_UP);
      mExchangeFlag = false;
    } else {
      BigDecimal division = BigDecimal.ONE;
      BigDecimal originalRate = mOriginalRate;
      if (mOriginalRate.compareTo(BigDecimal.ZERO) == 0) {
        List<Rate> value = mTransactionVM.getRateLD().getValue();
        for (Rate r : value) {
          if (r.getId().equals(firstCurrency + "USD")) {
            originalRate = r.getRate();
          }
          if (r.getId().equals(secondCurrency + "USD")) {
            division = r.getRate();
          }
        }
      }
      divide = originalRate.divide(division, 6, RoundingMode.HALF_UP);
      mExchangeFlag = true;
    }
    if (mOriginalRate.compareTo(BigDecimal.ZERO) == 0) {
      mOriginalRate = divide;
    }
    String text = secondCurrency + " /" + firstCurrency;
    exchangeTextInCardView.setText(text);
    exchangeEditText.setText(divide.toPlainString());
    exchangeEditText.addTextChangedListener(mWatcher);
  }

  @Override public void setDate(String date) {
    dataPickerTextView.setText(Utilities.getStringFromDate(Utilities.getDateFromString(date)));
  }
}
