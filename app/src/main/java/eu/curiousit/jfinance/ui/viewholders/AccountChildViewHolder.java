package eu.curiousit.jfinance.ui.viewholders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.ui.balance.AccountExpandableAdapter;
import java.util.List;

public class AccountChildViewHolder extends ChildViewHolder {

  @BindView(R.id.accountParentRecyclerView) RecyclerView mRecyclerView;
  private Context mContext;

  public AccountChildViewHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
    mContext = itemView.getContext();
  }

  public void bind(List<ParentListItem> mAccounts) {
    mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    mRecyclerView.setAdapter(new AccountExpandableAdapter(mContext, mAccounts, false));
  }
}