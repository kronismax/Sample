package eu.curiousit.jfinance.ui.operations;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.ExpenseCategory;
import eu.curiousit.jfinance.data.database.model.IncomeCategory;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.User;
import eu.curiousit.jfinance.utils.Constants;
import eu.curiousit.jfinance.utils.Utilities;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * It's so sad, and I try to optimize it.
 */
public class MultipleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<Transaction> mTransactionEntities;
  private Context mContext;
  private Callback mCallback;
  private List<Currency> mCurrencies;
  private List<Account> mAccounts;
  private List<IncomeCategory> mIncomeCategories;
  private List<ExpenseCategory> mExpenseCategories;
  private List<User> mUsers;
  private int mCurrentUserId;

  public MultipleAdapter(List<Transaction> transactionList, FragmentActivity activity,
      OperationsFragment operationsFragment, List<Currency> currencies, List<Account> accounts,
      List<IncomeCategory> incomeCategories, List<ExpenseCategory> expenseCategories, List<User> users,
      int currentUserId) {
    mTransactionEntities = transactionList;
    mContext = activity;
    mCallback = operationsFragment;
    mCurrencies = currencies;
    mAccounts = accounts;
    mIncomeCategories = incomeCategories;
    mExpenseCategories = expenseCategories;
    mUsers = users;
    mCurrentUserId = currentUserId;
  }

  public void setData(List<Transaction> transactionList, View v) {
    mTransactionEntities.clear();
    mTransactionEntities.addAll(transactionList);
    if (mTransactionEntities.isEmpty()) {
      Snackbar snackbar = Snackbar.make(v, R.string.list_is_empty, Snackbar.LENGTH_SHORT);
      snackbar.show();
    }
    notifyDataSetChanged();
  }

  @Override public int getItemViewType(int position) {
    Transaction transaction = mTransactionEntities.get(position);
    switch (transaction.getType()) {
      case Transaction.TYPE_INCOME:
        return Constants.TYPE_INCOME;
      case Transaction.TYPE_EXPENSE:
        return Constants.TYPE_EXPENSE;
      case Transaction.TYPE_EXCHANGE:
        return Constants.TYPE_EXCHANGE;
      case Transaction.TYPE_INTERNAL_TRANSFER:
        return Constants.TYPE_INTERNAL_TRANSFER;
      case Transaction.TYPE_EXTERNAL_TRANSFER:
        return Constants.TYPE_EXTERNAL_TRANSFER;
    }
    return position % 2 * 2;
  }

  @Override public int getItemCount() {
    return mTransactionEntities.size();
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView;
    switch (viewType) {
      case Constants.TYPE_INCOME:
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_income, parent, false);
        return new IncomeViewHolder(itemView);
      case Constants.TYPE_EXPENSE:
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expense, parent, false);
        return new ExpenseViewHolder(itemView);
      case Constants.TYPE_EXCHANGE:
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exchange, parent, false);
        return new ExchangeViewHolder(itemView);
      case Constants.TYPE_INTERNAL_TRANSFER:
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_internal_transfer, parent, false);
        return new InternalTransferViewHolder(itemView);
      case Constants.TYPE_EXTERNAL_TRANSFER:
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_external_transfer, parent, false);
        return new ExternalTransferViewHolder(itemView);
    }
    return null;
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    Transaction transaction = mTransactionEntities.get(holder.getAdapterPosition());
    String currencyCode = "";
    for (Currency currency : mCurrencies) {
      if (currency.getId() == transaction.getCurrencyId()) {
        currencyCode = currency.getCode();
      }
    }
    String destinationCurrencyCode = "";
    String categoryText = "";
    int destinationAccId;
    int accountId = transaction.getAccountId();
    String text1 = mContext.getString(R.string.from) + " " + getDescription(accountId);
    switch (holder.getItemViewType()) {
      case Constants.TYPE_INCOME:
        IncomeViewHolder incomeVH = (IncomeViewHolder) holder;

        incomeVH.mAmountIncomeTextView.setText(getAmount(transaction, currencyCode));
        for (int i = 0; i < mIncomeCategories.size(); i++) {
          IncomeCategory incomeCategory = mIncomeCategories.get(i);
          if (incomeCategory.getId().compareTo(transaction.getIncomeCategoryId()) == 0) {
            categoryText = incomeCategory.getDescription();
            break;
          }
        }
        incomeVH.mCategoryIncomeTextView.setText(categoryText);
        incomeVH.mDateIncomeTextView.setText(getStringFromDate(transaction));
        String text = mContext.getString(R.string.to) + " " + getDescription(accountId);
        incomeVH.mTargetIncomeTextView.setText(text);
        incomeVH.mDescriptionIncomeTextView.setText(transaction.getTransactionDescription());
        incomeVH.mContainer.setOnLongClickListener(new RevertClickListener(transaction));
        break;

      case Constants.TYPE_EXPENSE:
        ExpenseViewHolder expenseVH = (ExpenseViewHolder) holder;
        expenseVH.mAmountExpenseTextView.setText(getAmount(transaction, currencyCode));
        for (int i = 0; i < mExpenseCategories.size(); i++) {
          if (mExpenseCategories.get(i).getId().compareTo(transaction.getExpenseCategoryId()) == 0) {
            categoryText = mExpenseCategories.get(i).getDescription();
          }
        }
        expenseVH.mCategoryExpenseTextView.setText(categoryText);
        expenseVH.mDateExpenseTextView.setText(getStringFromDate(transaction));
        expenseVH.mTargetExpenseTextView.setText(text1);
        expenseVH.mDescriptionExpenseTextView.setText(transaction.getTransactionDescription());
        expenseVH.mContainer.setOnLongClickListener(new RevertClickListener(transaction));
        break;
      case Constants.TYPE_EXCHANGE:
        currencyCode = getCode(transaction);
        for (int i = 0; i < mCurrencies.size(); i++) {
          if (mCurrencies.get(i).getId().compareTo(transaction.getDestinationCurrencyId()) == 0) {
            destinationCurrencyCode = mCurrencies.get(i).getCode();
          }
        }
        ExchangeViewHolder exchangeVH = (ExchangeViewHolder) holder;
        String text2 = transaction.getDestinationAmount().setScale(2, RoundingMode.CEILING).toPlainString()
            + " "
            + destinationCurrencyCode;
        exchangeVH.mDestinationAmountText.setText(text2);
        exchangeVH.mAmountExchangeTextView.setText(getAmount(transaction, currencyCode));
        exchangeVH.mDateExchangeTextView.setText(getStringFromDate(transaction));
        String text3 = mContext.getString(R.string.exchange_rate)
            + " "
            + transaction.getRate().setScale(6, RoundingMode.CEILING)
            + " ("
            + currencyCode
            + "/"
            + destinationCurrencyCode
            + ")";
        exchangeVH.mRateExchangeTextView.setText(text3);
        destinationAccId = accountId;
        exchangeVH.mTargetExchangeTextView.setText(getDescription(destinationAccId));
        exchangeVH.mDescriptionExchangeTextView.setText(transaction.getTransactionDescription());
        exchangeVH.mContainer.setOnLongClickListener(new RevertClickListener(transaction));
        break;
      case Constants.TYPE_INTERNAL_TRANSFER:
        currencyCode = getCode(transaction);
        InternalTransferViewHolder internalTransVH = (InternalTransferViewHolder) holder;
        internalTransVH.mAmountInternalTransferTextView.setText(getAmount(transaction, currencyCode));
        internalTransVH.mDestinationTransferTextView.setText(text1);
        internalTransVH.mDateInternalTransferTextView.setText(getStringFromDate(transaction));
        destinationAccId = transaction.getDestinationAccountId();
        String text5 = mContext.getString(R.string.to) + " " + getDescription(destinationAccId);
        internalTransVH.mTargetInternalTransferTextView.setText(text5);
        internalTransVH.mCommentInternalTransferTextView.setText(transaction.getTransactionDescription());
        internalTransVH.mContainer.setOnLongClickListener(new RevertClickListener(transaction));
        break;
      case Constants.TYPE_EXTERNAL_TRANSFER:
        ExternalTransferViewHolder externalTransVH = (ExternalTransferViewHolder) holder;
        externalTransVH.mCommentExternalTransferTextView.setText(transaction.getTransactionDescription());
        currencyCode = getCode(transaction);
        externalTransVH.mAmountExternalTransferTextView.setText(getAmount(transaction, currencyCode));
        String destinationTransferText = "";
        externalTransVH.mDateExternalTransferTextView.setText(getStringFromDate(transaction));
        if (transaction.getTransactionStatus().equals(Transaction.STATUS_COMPLETED)
            || transaction.getTransactionStatus().equals(Transaction.STATUS_REVERTED)) {
          externalTransVH.mAcceptButton.setVisibility(Button.GONE);
          destinationAccId = transaction.getDestinationAccountId();
          if (transaction.getUserId() == mCurrentUserId) {
            destinationTransferText = mContext.getString(R.string.from) + " " + getDescription(accountId);
            String destinationAccount = "";
            for (User user : mUsers) {
              if (user.getId() == transaction.getDestinationUserId()) {
                destinationAccount = user.getName();
              }
            }
            String text4 = mContext.getString(R.string.to_user) + " " + destinationAccount;
            externalTransVH.mDestinationExternalTransferTextView.setText(text4);
          } else {
            destinationTransferText =
                mContext.getString(R.string.from_person) + " " + getUserName(transaction.getUserId());
            String text4 = mContext.getString(R.string.to) + "" + (hasAccountsKey(destinationAccId) ? getDescription(
                destinationAccId) : "?????");
            externalTransVH.mDestinationExternalTransferTextView.setText(text4);
          }
        } else if (transaction.getTransactionStatus()
            .equals(Transaction.STATUS_PENDING)) {
          if (transaction.getDestinationUserId() == mCurrentUserId) {
            externalTransVH.mAcceptButton.setText(mContext.getString(R.string.confirm));
            externalTransVH.mAcceptButton.setOnClickListener(v -> showAccountDialog(v, transaction, externalTransVH));
            destinationTransferText = mContext.getString(R.string.from_person) + getUserName(transaction.getUserId());
          } else {
            String destinationUserName = "";
            for (Account account : mAccounts) {
              if (account.getId() == accountId) {
                destinationUserName = account.getDescription();
              }
            }
            externalTransVH.mDestinationExternalTransferTextView.setVisibility(TextView.VISIBLE);
            String text4 = mContext.getString(R.string.from) + " " + destinationUserName;
            externalTransVH.mDestinationExternalTransferTextView.setText(text4);
            externalTransVH.mAcceptButton.setVisibility(Button.VISIBLE);
            externalTransVH.mAcceptButton.setText(mContext.getString(R.string.cancel));
            destinationTransferText = mContext.getString(R.string.to) + getUserName(transaction.getDestinationUserId());
            externalTransVH.mAcceptButton.setOnClickListener(v -> {
              transaction.setTransactionStatus(Transaction.STATUS_VOID);

              mCallback.updateTransaction(transaction)
                  .subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(baseResponse -> {
                    externalTransVH.mAcceptButton.setVisibility(View.GONE);
                    mTransactionEntities.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), mTransactionEntities.size());
                  }, throwable -> {
                    Snackbar snackbar = Snackbar.make(v, throwable.getMessage(), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                  });
            });
          }
        }
        externalTransVH.mDestinationTransferTextView.setText(destinationTransferText);
        externalTransVH.mContainer.setOnLongClickListener(new RevertClickListener(transaction));
        break;
    }
  }

  private boolean hasAccountsKey(int destinationAccId) {
    for (Account account : mAccounts) {
      if (account.getId().compareTo(destinationAccId) == 0) {
        return true;
      }
    }
    return false;
  }

  private String getUserName(int userId) {
    for (int i = 0; i < mUsers.size(); i++) {
      if (mUsers.get(i).getId().compareTo(userId) == 0) {
        return mUsers.get(i).getName();
      }
    }
    return "";
  }

  private String getCode(Transaction transaction) {
    for (Currency currency : mCurrencies) {
      if (currency.getId().compareTo(transaction.getCurrencyId()) == 0) {
        return currency.getCode();
      }
    }
    return "";
  }

  private String getDescription(int destinationAccountId) {
    for (int i = 0; i < mAccounts.size(); i++) {
      if (mAccounts.get(i).getId().compareTo(destinationAccountId) == 0) {
        return mAccounts.get(i).getDescription();
      }
    }
    return "";
  }

  private String getStringFromDate(Transaction transaction) {
    return Utilities.getStringFromDate(Utilities.longToDate(transaction.getDateTime()));
  }

  @NonNull private String getAmount(Transaction transaction, String currencyCode) {
    return transaction.getAmount().setScale(2, RoundingMode.CEILING).toPlainString() + " " + currencyCode;
  }

  class IncomeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.container) CardView mContainer;
    @BindView(R.id.amount_income_text_view) TextView mAmountIncomeTextView;
    @BindView(R.id.category_income_text_view) TextView mCategoryIncomeTextView;
    @BindView(R.id.date_income_text_view) TextView mDateIncomeTextView;
    @BindView(R.id.target_income_text_view) TextView mTargetIncomeTextView;
    @BindView(R.id.comment_income_text_view) TextView mDescriptionIncomeTextView;

    public IncomeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  class ExpenseViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.container) CardView mContainer;
    @BindView(R.id.amount_expense_text_view) TextView mAmountExpenseTextView;
    @BindView(R.id.category_expense_text_view) TextView mCategoryExpenseTextView;
    @BindView(R.id.date_expense_text_view) TextView mDateExpenseTextView;
    @BindView(R.id.target_expense_text_view) TextView mTargetExpenseTextView;
    @BindView(R.id.comment_expense_text_view) TextView mDescriptionExpenseTextView;

    public ExpenseViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  class ExchangeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.container) CardView mContainer;
    @BindView(R.id.amount_exchange_text_view) TextView mAmountExchangeTextView;
    @BindView(R.id.rate_exchange_text_view) TextView mRateExchangeTextView;
    @BindView(R.id.date_exchange_text_view) TextView mDateExchangeTextView;
    @BindView(R.id.destination_amount_text) TextView mDestinationAmountText;
    @BindView(R.id.target_exchange_text_view) TextView mTargetExchangeTextView;
    @BindView(R.id.comment_exchange_text_view) TextView mDescriptionExchangeTextView;

    public ExchangeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  class InternalTransferViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.container) CardView mContainer;
    @BindView(R.id.amount_internal_transfer_text_view) TextView mAmountInternalTransferTextView;
    @BindView(R.id.internal_destination_transfer_text_view) TextView mDestinationTransferTextView;
    @BindView(R.id.date_internal_transfer_text_view) TextView mDateInternalTransferTextView;
    @BindView(R.id.target_internal_transfer_text_view) TextView mTargetInternalTransferTextView;
    @BindView(R.id.comment_internal_text_view) TextView mCommentInternalTransferTextView;

    public InternalTransferViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  class ExternalTransferViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.container) CardView mContainer;
    @BindView(R.id.amount_external_transfer_text_view) TextView mAmountExternalTransferTextView;
    @BindView(R.id.internal_destination_transfer_text_view) TextView mDestinationTransferTextView;
    @BindView(R.id.date_external_transfer_text_view) TextView mDateExternalTransferTextView;
    @BindView(R.id.target_external_transfer_text_view) TextView mDestinationExternalTransferTextView;
    @BindView(R.id.accept_button) Button mAcceptButton;
    @BindView(R.id.comment_external_text_view) TextView mCommentExternalTransferTextView;

    public ExternalTransferViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  private void showAccountDialog(View v, Transaction transaction, ExternalTransferViewHolder holder) {
    ArrayList<String> accountList = new ArrayList<>();
    for (Account account : mAccounts) {
      if (account.getUserId() == mCurrentUserId) {
        accountList.add(account.getDescription());
      }
    }
    new MaterialDialog.Builder(v.getContext()).title(mContext.getString(R.string.select_title))
        .items(accountList)
        .itemsCallbackSingleChoice(0, (dialog, view, which, text) -> {
          for (Account account : mAccounts) {
            if (account.getDescription().equals(text)) {
              transaction.setDestinationAccountId(account.getId());
            }
          }
          transaction.setDestinationAmount(transaction.getAmount());
          transaction.setDestinationCurrencyId(transaction.getCurrencyId());
          transaction.setTransactionStatus(Transaction.STATUS_COMPLETED);
          mCallback.updateTransaction(transaction)
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(baseResponse -> holder.mAcceptButton.setVisibility(View.GONE), throwable -> {
                Snackbar snackbar = Snackbar.make(v, throwable.getMessage(), Snackbar.LENGTH_SHORT);
                snackbar.show();
              });
          return true;
        })
        .positiveText(mContext.getString(R.string.ok_text))
        .negativeText(mContext.getString(R.string.cancel))
        .show();
  }

  private class RevertClickListener implements View.OnLongClickListener {

    private Transaction mTransaction;

    RevertClickListener(Transaction transaction) {
      mTransaction = transaction;
    }

    @Override public boolean onLongClick(View v) {
      if (mTransaction.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
        Snackbar snackbar = Snackbar.make(v, R.string.only_positive_transaction_can_revert, Snackbar.LENGTH_SHORT);
        snackbar.show();
      } else if (mTransaction.getTransactionStatus().equals(Transaction.STATUS_COMPLETED)) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(R.string.are_you_sure_to_duplicate);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("Ok", (arg0, arg1) -> {
          mTransaction.setAmount(mTransaction.getAmount().multiply(new BigDecimal(-1)));
          mCallback.insertTransaction(mTransaction)
              .observeOn(AndroidSchedulers.mainThread())
              .subscribeOn(Schedulers.io())
              .subscribe(response -> {
                if (response.getSuccessful()) {
                  Toast.makeText(mContext, R.string.transaction_completed, Toast.LENGTH_SHORT).show();
                  mCallback.update();
                } else {
                  Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();
                }
              });
        });
        alertDialogBuilder.setNegativeButton("Cancel", null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
      } else {
        Snackbar snackbar = Snackbar.make(v, R.string.you_can_cancel_transaction, Snackbar.LENGTH_SHORT);
        snackbar.show();
      }
      return false;
    }
  }
}
