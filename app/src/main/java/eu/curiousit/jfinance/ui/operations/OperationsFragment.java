package eu.curiousit.jfinance.ui.operations;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.afollestad.materialdialogs.MaterialDialog;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Account;
import eu.curiousit.jfinance.data.database.model.Currency;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.data.database.model.base.BaseResponse;
import eu.curiousit.jfinance.data.database.model.base.InsertResponse;
import eu.curiousit.jfinance.data.database.model.response.UserDataResponse;
import eu.curiousit.jfinance.utils.Utilities;
import io.reactivex.Observable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public class OperationsFragment extends LifecycleFragment implements View.OnClickListener, Callback {

  private static final String TAG = "OperationsFragment";

  @BindView(R.id.operations_recycler_view) RecyclerView mMultipleRecyclerView;
  @BindView(R.id.swipe_to_refresh_layout_operations) SwipeRefreshLayout mRefreshLayout;
  private BottomSheetDialog mBottomSheetDialog;
  private TextView mSelectTypeTextView;
  private Switch mSelectTypeSwitch;
  private TextView mSelectCurrencyTextView;
  private Switch mSelectCurrencySwitch;
  private TextView mSelectDateTextView;
  private Switch mSelectDateSwitch;
  private TextView mSelectAccountTextView;
  private Switch mSelectAccountSwitch;
  private List<String> mTypeList;
  private Unbinder mUnbinder;
  @BindString(R.string.select_title) String selectTitleString;
  @BindString(R.string.income_title) String incomeString;
  @BindString(R.string.expense_title) String expenseString;
  @BindString(R.string.exchange_title) String exchangeString;
  @BindString(R.string.internal_transfer_title) String internalTransferString;
  @BindString(R.string.external_transfer_title) String externalTransferString;
  @BindString(R.string.point) String pointString;
  @BindString(R.string.account_filter) String mSelectAccount;
  @BindString(R.string.select_date) String mSelectDate;
  @BindArray(R.array.date_filter) String[] mDateArray;
  private String selectTypeString;
  private ArrayList<String> currencyList;
  private SparseArray<String> selectCurrencyMap = new SparseArray<>();
  private SparseArray<String> selectAccountMap = new SparseArray<>();
  private int selectCurrencyId;
  private int selectAccountId;
  private String mSelectDateString;
  private MultipleAdapter mMultipleAdapter;
  private LinearLayoutManager mLayout;
  private boolean isLoading;
  private boolean isPagination;
  private boolean isLastTransaction;

  private OperationsVM mOperationsVM;
  private List<Currency> mCurrenciesList;
  private List<Account> mAccountsList;
  private UserDataResponse userData;

  public OperationsFragment() {
    // Required empty public constructor
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    getActivity().setTitle(R.string.operations);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getActivity().setTitle(R.string.operations);
    View view = inflater.inflate(R.layout.fragment_operations, container, false);
    mUnbinder = ButterKnife.bind(this, view);

    initViewModel();
    mRefreshLayout.setOnRefreshListener(() -> {
      isLastTransaction = false;
      updateTransactions();
    });
    mRefreshLayout.setColorSchemeResources(R.color.colorAccent);

    return view;
  }

  private void initViewModel() {
    mOperationsVM = ViewModelProviders.of(getActivity()).get(OperationsVM.class);
    mOperationsVM.getCurrenciesLD().observe(this, currencies -> {
      mCurrenciesList = currencies;
      OperationsFragment.this.addStringToLists(currencies);
    });
    mOperationsVM.getAccountsLD().observe(this, accounts -> mAccountsList = accounts);
    mOperationsVM.getLoginResponse().observe(this, loginResponse -> {
      if (loginResponse != null) {
        userData = loginResponse.getData();
      }
    });
    mOperationsVM.getLoadingLD().observe(this, isLoading -> {
      if (isLoading) {
        mRefreshLayout.setRefreshing(true);
      } else {
        if (mRefreshLayout.isRefreshing()) {
          mRefreshLayout.setRefreshing(false);
        }
      }
    });
    mOperationsVM.getDataPresent().observe(this, aBoolean -> updateView());
    mOperationsVM.getTransactionsLD().observe(this, transactions -> updateView());
  }

  public void updateTransactions() {
    mOperationsVM.updateData();
  }

  public void updateView() {
    mMultipleAdapter =
        new MultipleAdapter(getTransactionList(), getActivity(), OperationsFragment.this, mCurrenciesList,
            mAccountsList, mOperationsVM.getIncomeCategoriesLD().getValue(),
            mOperationsVM.getExpenseCategoriesLD().getValue(), mOperationsVM.getUsersLD().getValue(),
            mOperationsVM.getLoginResponse().getValue().getData().getUser().getId());
    mMultipleRecyclerView.setAdapter(mMultipleAdapter);
    if (mMultipleRecyclerView.getLayoutManager() == null) {
      mLayout = new LinearLayoutManager(getActivity());
      mMultipleRecyclerView.setLayoutManager(mLayout);
    }
    mMultipleRecyclerView.addOnScrollListener(recyclerViewOnScrollListener);
    if (mRefreshLayout.isRefreshing()) {
      mRefreshLayout.setRefreshing(false);
    }
  }

  @Override public void onResume() {
    super.onResume();
    getActivity().setTitle(R.string.operations);
    if (getArguments() != null && getArguments().getBoolean("update")) {
      mOperationsVM.loadTransactions();
    }
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mUnbinder.unbind();
  }

  private List<Transaction> getTransactionList() {
    List<Transaction> transactions = mOperationsVM.getTransactionsLD().getValue();
    List<Currency> mCurrenciesList = mOperationsVM.getCurrenciesLD().getValue();
    if (transactions == null || mCurrenciesList == null) {
      return new ArrayList<>();
    }

    List<Transaction> transactionList = new ArrayList<>();
    int adapterItemCount = 0;
    if (mMultipleAdapter != null) {
      adapterItemCount = mMultipleAdapter.getItemCount();
    }
    Date currentDate = Utilities.getDateFromString(Utilities.getCurrentDate());
    for (Transaction transaction : transactions) {

      if (!TextUtils.isEmpty(selectTypeString)
          && !transaction.getType().equals(selectTypeString)
          && mSelectTypeSwitch.isChecked()) {
        continue;
      }
      if (selectCurrencyMap.get(selectCurrencyId) != null
          && transaction.getCurrencyId() != selectCurrencyId
          && mSelectCurrencySwitch.isChecked()) {
        continue;
      }
      Switch selectAccountSwitch = mSelectAccountSwitch;
      if (selectAccountMap.get(selectAccountId) != null
          && transaction.getAccountId() != selectAccountId
          && transaction.getDestinationAccountId() != selectAccountId
          && selectAccountSwitch.isChecked()) {
        continue;
      }
      boolean b = transaction.getUserId() != userData.getUser().getId();
      if (transaction.getType().equals(Transaction.TYPE_EXTERNAL_TRANSFER)
          && transaction.getDestinationUserId() == userData.getUser().getId()) {
        b = false;
      }
      if (b && selectAccountSwitch == null) {
        continue;
      } else if (b && (!selectAccountSwitch.isChecked() || selectAccountId == 0)) {
        continue;
      }
      long daysBetween = daysBetween(Utilities.longToDate(transaction.getDateTime()), currentDate);

      if (!TextUtils.isEmpty(mSelectDateString) && mSelectDateSwitch.isChecked()) {
        if (mSelectDateString.equals(mDateArray[0])) {
          if (daysBetween > 365) {
            continue;
          } else {
            transactionList.add(transaction);
            continue;
          }
        } else if (mSelectDateString.equals(mDateArray[1])) {
          if (daysBetween > 30) {
            continue;
          } else {
            transactionList.add(transaction);
            continue;
          }
        } else if (mSelectDateString.equals(mDateArray[2])) {
          if (daysBetween > 7) {
            continue;
          } else {
            transactionList.add(transaction);
            continue;
          }
        }
      } else {
        boolean b1 = !isPagination;
        boolean listSize = transactionList.size() >= adapterItemCount + 20;
        boolean b2 = b1 || listSize;
        boolean b3 = daysBetween > 30 && b2;
        if (b3) {
          continue;
        }
      }
      transactionList.add(transaction);
    }
    if (mMultipleAdapter != null && mMultipleAdapter.getItemCount() == transactionList.size() + 20) {
      isLastTransaction = true;
    }
    return transactionList;
  }

  private static long daysBetween(Date dateEarly, Date dateLater) {
    Calendar cal1 = Calendar.getInstance();
    cal1.setTime(dateEarly);
    Calendar cal2 = Calendar.getInstance();
    cal2.setTime(dateLater);

    long endL = cal2.getTimeInMillis() + cal2.getTimeZone().getOffset(cal2.getTimeInMillis());
    long startL = cal1.getTimeInMillis() + cal1.getTimeZone().getOffset(cal1.getTimeInMillis());
    return (endL - startL) / (24 * 60 * 60 * 1000);
  }

  private void addStringToLists(List<Currency> currencies) {
    mTypeList = new ArrayList<>();
    mTypeList.add(incomeString);
    mTypeList.add(expenseString);
    mTypeList.add(exchangeString);
    mTypeList.add(internalTransferString);
    mTypeList.add(externalTransferString);

    currencyList = new ArrayList<>();
    for (Currency currencyEntity : currencies) {
      currencyList.add(currencyEntity.getCode());
    }
  }

  private void showDialog(String title, List<String> list, final TextView textView) {
    new MaterialDialog.Builder(
        getActivity()) // TODO: 05.07.16 https://medium.com/freenet-engineering/memory-leaks-in-android-identify-treat-and-avoid-d0b1233acc8#.pxak13g5i
        .title(selectTitleString + title).items(list).itemsCallbackSingleChoice(0, (dialog, view, which, text) -> {
      setChooseData(textView, text);
      return true;
    }).positiveText(R.string.ok_text).negativeText(R.string.cancel).show();
  }

  private void setChooseData(TextView textView, CharSequence text) {
    if (mSelectAccountSwitch != null && !mSelectAccountSwitch.isChecked()) {
      selectAccountId = 0;
    }
    String text1 = text + " " + pointString;
    switch (textView.getId()) {
      case R.id.select_type_text_view:
        mSelectTypeTextView.setText(text1);
        selectTypeString = convertStringType((String) text);
        mSelectTypeSwitch.setChecked(true);
        break;
      case R.id.select_currency_text_view:
        mSelectCurrencyTextView.setText(text1);
        for (Currency currency : mCurrenciesList) {
          if (currency.getCode().equals(text)) {
            selectCurrencyId = currency.getId();
          }
        }
        selectCurrencyMap.put(selectCurrencyId, String.valueOf(text));
        mSelectCurrencySwitch.setChecked(true);
        break;
      case R.id.select_date_text_view:
        mSelectDateString = String.valueOf(text);
        mSelectDateTextView.setText(text1);
        mSelectDateSwitch.setChecked(true);
        break;
      case R.id.select_account_text_view:
        mSelectAccountTextView.setText(text1);
        for (Account account : mAccountsList) {
          if (account.getDescription().equals(text)) {
            selectAccountId = account.getId();
          }
        }
        selectAccountMap.put(selectAccountId, String.valueOf(text));
        mSelectAccountSwitch.setChecked(true);
        break;
    }
  }

  private String convertStringType(String text) {
    if (text.equals(incomeString)) {
      return Transaction.TYPE_INCOME;
    } else if (text.equals(exchangeString)) {
      return Transaction.TYPE_EXCHANGE;
    } else if (text.equals(expenseString)) {
      return Transaction.TYPE_EXPENSE;
    } else if (text.equals(internalTransferString)) {
      return Transaction.TYPE_INTERNAL_TRANSFER;
    } else if (text.equals(externalTransferString)) {
      return Transaction.TYPE_EXTERNAL_TRANSFER;
    }
    return "";
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
    super.onCreateOptionsMenu(menu, menuInflater);
    if (menu.size() > 0) {
      menu.clear();
    }
    menuInflater.inflate(R.menu.main, menu);
    for (int i = 0; i < menu.size(); i++) {
      MenuItem item = menu.getItem(i);
      View itemChooser = item.getActionView();
      if (itemChooser != null) {
        itemChooser.setOnClickListener(this);
      }
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.action_filter) {
      showFilterDialog();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void showFilterDialog() {
    if (mBottomSheetDialog != null) {
      mBottomSheetDialog.show();
      return;
    }
    mBottomSheetDialog = new BottomSheetDialog(getActivity());
    mBottomSheetDialog.setOnCancelListener(dialog -> mBottomSheetDialog = null);
    @SuppressLint("InflateParams") View sheetView = getActivity().getLayoutInflater().inflate(R.layout.fragment_bottom_sheet, null);
    mBottomSheetDialog.setContentView(sheetView);
    CardView selectTypeCardView = sheetView.findViewById(R.id.select_type_card_view);
    CardView acceptCardView = sheetView.findViewById(R.id.accept_card_view);

    selectTypeCardView.setOnClickListener(v -> showDialog(getString(R.string.type), mTypeList, mSelectTypeTextView));
    mSelectTypeTextView = sheetView.findViewById(R.id.select_type_text_view);
    mSelectTypeSwitch = sheetView.findViewById(R.id.select_type_switch);
    acceptCardView.setOnClickListener(v -> {
      isLastTransaction = false;
      mMultipleAdapter.setData(getTransactionList(), OperationsFragment.this.getView());
      mBottomSheetDialog.hide();
    });

    CardView selectCurrencyCardView = sheetView.findViewById(R.id.select_currency_card_view);
    mSelectCurrencyTextView = sheetView.findViewById(R.id.select_currency_text_view);
    selectCurrencyCardView.setOnClickListener(
        v -> showDialog(getString(R.string.currency), currencyList, mSelectCurrencyTextView));
    mSelectCurrencySwitch = sheetView.findViewById(R.id.select_currency_switch);

    CardView selectDateCardView = sheetView.findViewById(R.id.select_date_card_view);
    mSelectDateTextView = sheetView.findViewById(R.id.select_date_text_view);
    selectDateCardView.setOnClickListener(v -> showDialog(mSelectDate, Arrays.asList(mDateArray), mSelectDateTextView));
    mSelectDateSwitch = sheetView.findViewById(R.id.select_date_switch);

    CardView selectAccountCardView = sheetView.findViewById(R.id.select_account_card_view);
    mSelectAccountTextView = sheetView.findViewById(R.id.select_account_text_view);
    selectAccountCardView.setOnClickListener(v -> {
      ArrayList<String> list = new ArrayList<>();
      for (Account accountEntity : mAccountsList) {
        if (userData.getUser().getId() == accountEntity.getUserId() || userData.getUser().getAdmin()) {
          list.add(accountEntity.getDescription());
        }
      }
      showDialog(mSelectAccount, list, mSelectAccountTextView);
    });
    mSelectAccountSwitch = sheetView.findViewById(R.id.select_account_switch);

    mBottomSheetDialog.show();
  }

  @Override public void onClick(View v) {
    showFilterDialog();
  }

  private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
    @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
      super.onScrollStateChanged(recyclerView, newState);
    }

    @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
      super.onScrolled(recyclerView, dx, dy);
      int visibleItemCount = mLayout.getChildCount();
      int totalItemCount = mLayout.getItemCount();
      int firstVisibleItemPosition = mLayout.findFirstVisibleItemPosition();

      if (!isLoading && !isLastTransaction) {
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
            && firstVisibleItemPosition >= 0
            && totalItemCount >= PAGE_SIZE) {
          addTransactions();
        }
      }
    }
  };

  private void addTransactions() {
    isLoading = true;
    isPagination = true;
    if (!mRefreshLayout.isRefreshing()) {
      mRefreshLayout.setRefreshing(true);
      mMultipleAdapter.setData(getTransactionList(), getView());
      mRefreshLayout.setRefreshing(false);
    }
    isLoading = false;
    isPagination = false;
  }

  @Override public Observable<BaseResponse> updateTransaction(Transaction transaction) {
    return mOperationsVM.updateTransaction(transaction);
  }

  @Override public Observable<InsertResponse> insertTransaction(Transaction transaction) {
    return mOperationsVM.insertTransactions(transaction);
  }

  @Override public void update() {
    mOperationsVM.loadTransactions();
  }
}
