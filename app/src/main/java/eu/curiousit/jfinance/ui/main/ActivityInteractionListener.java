package eu.curiousit.jfinance.ui.main;

public interface ActivityInteractionListener {
    void operationsFragmentTransaction();
}
