package eu.curiousit.jfinance.ui.base;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import eu.curiousit.jfinance.data.response.Response;

public class TransactionVM extends SharedVM {

  @NonNull private MutableLiveData<Boolean> mDataPresent = new MutableLiveData<>();

  public TransactionVM() {
    if (loginResponse.getValue() == null) {
      checkSession(userDataResponse -> {
        loginResponse.setValue(Response.success(userDataResponse));
        checkDataPresent();
      });
    } else {
      checkDataPresent();
    }
  }

  private void checkDataPresent() {
    if (currenciesLD.getValue() == null
        || accountsLD.getValue() == null
        || usersLD.getValue() == null
        || incomeCategoriesLD.getValue() == null
        || incomeContragentLD.getValue() == null
        || expenseContragentLD.getValue() == null
        || expenseCategoriesLD.getValue() == null) {
      loadTransactionsData(TransactionVM.this::setDataPresent);
    } else {
      setDataPresent();
    }
  }

  private void setDataPresent() {
    mDataPresent.setValue(true);
  }

  @NonNull public MutableLiveData<Boolean> getDataPresent() {
    return mDataPresent;
  }
}
