package eu.curiousit.jfinance.ui.balance;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import eu.curiousit.jfinance.R;
import java.util.List;

public class BalanceFragment extends LifecycleFragment {

  @BindView(R.id.account_recycler_view) RecyclerView mAccountRecyclerView;
  @BindView(R.id.total_recycler_view) RecyclerView mTotalRecyclerView;
  @BindView(R.id.refresh_layout_balance) SwipeRefreshLayout mRefreshLayout;
  private ExpandableRecyclerAdapter mAdapter;
  private Unbinder mUnbinder;
  private BalanceVM mViewModel;

  public BalanceFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_balance, container, false);
    mUnbinder = ButterKnife.bind(this, view);

    initRecyclerView();

    mViewModel = ViewModelProviders.of(getActivity()).get(BalanceVM.class);
    // TODO: 8/31/17 желательно эту всю логику выносить куда то, например в презентер или viewModel
    mViewModel.getBalance().observe(this, accountsResponse -> {
      if (mRefreshLayout.isRefreshing()) {
        mRefreshLayout.setRefreshing(false);
      }
      if (accountsResponse != null) {
        Throwable error = accountsResponse.getError();
        if (error != null) {
          Snackbar.make(view, error.getMessage(), Snackbar.LENGTH_SHORT).show();
        }

        List<ParentListItem> data = accountsResponse.getData();
        if (data != null && !data.isEmpty()) {
          if (mViewModel.getLoginResponse().getValue().getData().getUser().getAdmin()) {
            mAdapter = new UsersExpandableAdapter(getActivity(), data);
          } else {
            mAdapter = new AccountExpandableAdapter(getActivity(), data, true);
          }
          mAccountRecyclerView.setAdapter(mAdapter);
        }
      }
    });
    mViewModel.getBalanceTotalLD().observe(this, totalResponse -> {
      if (totalResponse != null && totalResponse.getData() != null && !totalResponse.getData().isEmpty()) {
        TotalAdapter mTotalAdapter = new TotalAdapter(totalResponse.getData());
        mTotalRecyclerView.setAdapter(mTotalAdapter);
      }
    });

    mViewModel.getLoadingLD().observe(this, isLoading -> {
      if (isLoading != null && isLoading) {
        mRefreshLayout.setRefreshing(true);
      } else {
        mRefreshLayout.setRefreshing(false);
      }
    });
    return view;
  }

  private void initRecyclerView() {
    mAccountRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mTotalRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mRefreshLayout.setOnRefreshListener(() -> mViewModel.getBalances());
    mRefreshLayout.setColorSchemeResources(R.color.colorAccent);
  }

  @Override public void onResume() {
    super.onResume();
    if (getArguments() != null && getArguments().getBoolean("update")) {
      mViewModel.getBalances();
    }
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mUnbinder.unbind();
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    if (mAdapter != null) {
      mAdapter.onSaveInstanceState(outState);
    }
  }

  @Override public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
    super.onViewStateRestored(savedInstanceState);
    if (savedInstanceState != null && mAdapter != null) {
      mAdapter.onRestoreInstanceState(savedInstanceState);
    }
  }
}
