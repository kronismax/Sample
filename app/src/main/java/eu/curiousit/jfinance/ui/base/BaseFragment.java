package eu.curiousit.jfinance.ui.base;

import android.app.Activity;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;
import eu.curiousit.jfinance.R;
import eu.curiousit.jfinance.data.database.model.Transaction;
import eu.curiousit.jfinance.ui.main.ActivityInteractionListener;
import eu.curiousit.jfinance.utils.Utilities;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.Calendar;
import java.util.Date;

import static eu.curiousit.jfinance.utils.Constants.REQUEST_DATE;

public abstract class BaseFragment extends LifecycleFragment {

  private ActivityInteractionListener mActivityInteractionListener;
  protected String dateString;
  protected TransactionVM mTransactionVM;

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof ActivityInteractionListener) {
      mActivityInteractionListener = (ActivityInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    dateString = Utilities.getCurrentDate();
  }

  @Override public void onDetach() {
    super.onDetach();
    mActivityInteractionListener = null;
  }

  protected void initViewModel() {
    mTransactionVM = ViewModelProviders.of(getActivity()).get(TransactionVM.class);
    mTransactionVM.getErrorLD().observe(this, error -> {
      if (error != null && error.getMessage() != null) {
        showSnackbar(error.getMessage());
      }
    });
  }

  private void showSnackbar(String text) {
    Snackbar.make(getActivity().findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT).show();
  }

  private void goToOperationsFragment() {
    mActivityInteractionListener.operationsFragmentTransaction();
  }

  protected void insertTransaction(Transaction transaction) {
    mTransactionVM.insertTransactions(transaction)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(response -> {
          if (response.getSuccessful()) {
            showSnackbar(response.getMessage());
            goToOperationsFragment();
          } else {
            showSnackbar(response.getMessage());
          }
        });
  }

  protected void openWeightPicker() {
    DialogFragment fragment = new DateDialogFragment();
    fragment.setTargetFragment(this, REQUEST_DATE);
    fragment.show(getFragmentManager(), fragment.getClass().getName());
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_DATE) {
      String stringExtra = data.getStringExtra(DateDialogFragment.TAG_DATE_SELECTED);
      Calendar selectedCalendar = Calendar.getInstance();
      Date dateFromString = Utilities.getDateFromString(stringExtra);
      selectedCalendar.setTime(dateFromString);
      long time1 = getTime(selectedCalendar);
      Calendar instance = Calendar.getInstance();
      long time = getTime(instance);
      if (time1 > time) {
        showToast(R.string.future_date_error);
      } else {
        dateString = stringExtra;
        setDate(dateString);
      }
    }
  }

  public abstract void setDate(String date);

  public void showToast(@StringRes int id) {
    Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
  }

  protected long getTime(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime().getTime();
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    if (menu.size() > 0) {
      menu.clear();
    }
  }
}
