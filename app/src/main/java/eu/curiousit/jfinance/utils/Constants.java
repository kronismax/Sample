package eu.curiousit.jfinance.utils;

public class Constants {

  private Constants() {
  }

  public static final int REQUEST_DATE = 1;
  public static final int TYPE_INCOME = 0;
  public static final int TYPE_EXPENSE = 1;
  public static final int TYPE_EXCHANGE = 2;
  public static final int TYPE_INTERNAL_TRANSFER = 3;
  public static final int TYPE_EXTERNAL_TRANSFER = 4;
  public static final String SESSION_UUID = "sessionUUID";
  public static final String IS_ADMIN = "isAdmin";
}
