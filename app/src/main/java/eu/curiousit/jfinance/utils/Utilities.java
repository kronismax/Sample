package eu.curiousit.jfinance.utils;

import android.widget.DatePicker;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utilities {

  private Utilities() {
  }

  private static final String PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
  private static final String PATTERN_FOR_VIEW = "dd-MM-yyyy";
  private static final int NUMBER_OF_NUMBERS = 9;

  public static String getDateStringFromDatePicker(DatePicker datePicker) {
    int day = datePicker.getDayOfMonth();
    int month = datePicker.getMonth();
    int year = datePicker.getYear();

    Date date = new Date();

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(year, month, day, date.getHours(), date.getMinutes());
    SimpleDateFormat format = new SimpleDateFormat(PATTERN, Locale.getDefault());
    return format.format(calendar.getTime());
  }

  public static Date getDateFromString(String string) {
    DateFormat dateFormat = new SimpleDateFormat(PATTERN, Locale.getDefault());
    Date date = null;
    try {
      date = dateFormat.parse(string);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }

  public static String getCurrentDate() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat format = new SimpleDateFormat(PATTERN, Locale.getDefault());
    return format.format(calendar.getTime());
  }

  public static String getCurrentDateForView() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat format = new SimpleDateFormat(PATTERN_FOR_VIEW, Locale.getDefault());
    return format.format(calendar.getTime());
  }

  public static String getStringFromDate(Date date) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_FOR_VIEW, Locale.getDefault());
    return dateFormat.format(date);
  }

  public static String MD5(String md5) {
    try {
      java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
      byte[] array = md.digest(md5.getBytes());
      StringBuilder sb = new StringBuilder();
      for (byte anArray : array) {
        sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
      }
      return sb.toString();
    } catch (java.security.NoSuchAlgorithmException ignored) {
    }
    return null;
  }

  public static String formatString(String string) {
    String[] splittedString = string.split("\\.");

    String numberBeforePoint = splittedString[0];
    int lengthBeforePoint = numberBeforePoint.length();
    StringBuilder builder = new StringBuilder("");
    if (lengthBeforePoint < NUMBER_OF_NUMBERS) {

      for (int i = NUMBER_OF_NUMBERS, k = numberBeforePoint.length(), m = 0; i > 0; i--) {

        if (k == i) {
          builder.append(numberBeforePoint.charAt(m++));
          k--;
          if (i == 4 || i == 7) {
            builder.append(' ');
          }
        } else {
          builder.append(' ');
        }
      }
    } else if (lengthBeforePoint == NUMBER_OF_NUMBERS) {
      builder.append(numberBeforePoint.substring(0, 3))
          .append(' ')
          .append(numberBeforePoint.substring(3, 6))
          .append(' ')
          .append(numberBeforePoint.substring(6, 9));
    }
    return builder.toString();
  }

  public static Date longToDate(long value) {
    return new Date(value);
  }
}
