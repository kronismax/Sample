package eu.curiousit.jfinance.utils;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ObservableUtils<T> {

  public void getObservable(Single<T> f, Consumer<T> o) {
    Single single = f.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    single.subscribe(o);
  }
}
