package eu.curiousit.jfinance.entities;

public class CurrencyEntity {
  private int mId;
  private String mCode;
  private String mName;

  public CurrencyEntity() {
    super();
  }

  public CurrencyEntity(int id, String code, String name) {
    mId = id;
    mCode = code;
    mName = name;
  }

  public CurrencyEntity(String code, String name) {
    mCode = code;
    mName = name;
  }

  public void setId(int id) {
    mId = id;
  }

  public void setCode(String code) {
    mCode = code;
  }

  public void setName(String name) {
    mName = name;
  }

  public int getId() {
    return mId;
  }

  public String getName() {
    return mName;
  }

  public String getCode() {
    return mCode;
  }
}
