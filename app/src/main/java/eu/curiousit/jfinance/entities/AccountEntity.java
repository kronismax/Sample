package eu.curiousit.jfinance.entities;

public class AccountEntity {
  private int mId;
  private int mUserId;
  private boolean mCanBeNegative;
  private String mDescription;

  public AccountEntity() {
    super();
  }

  public AccountEntity(int id, int userId, boolean canBeNegative, String description) {
    mId = id;
    mUserId = userId;
    mCanBeNegative = canBeNegative;
    mDescription = description;
  }

  public void setId(int id) {
    mId = id;
  }

  public void setUserId(int userId) {
    mUserId = userId;
  }

  public void setCanBeNegative(boolean canBeNegative) {
    mCanBeNegative = canBeNegative;
  }

  public void setDescription(String description) {
    mDescription = description;
  }

  public int getId() {
    return mId;
  }

  public int getUserId() {
    return mUserId;
  }

  public boolean isCanBeNegative() {
    return mCanBeNegative;
  }

  public String getDescription() {
    return mDescription;
  }
}
